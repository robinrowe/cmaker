#!/bin/bash
# Created 2021/09/27 by Robin Rowe
# License MIT open source

Clean() 
{	for d in *; do
	if [ -d "$d" ]; then
		(cd -- "$d" && Clean)
	fi
	rm -f CMakeLists.txt sources.cmake *.tmp
	done
}

main()
{	Clean
}

main