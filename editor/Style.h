// Style.h 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#ifndef Style_h
#define Style_h

#include <iostream>

class Style
{	Style(const Style&) = delete;
	void operator=(const Style&) = delete;

public:
	~Style()
	{}
	Style()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Style& style)
{	return style.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Style& style)
{	return style.Input(is);
}

#endif
