// EditorWindow.h 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#ifndef EditorWindow_h
#define EditorWindow_h

#include <iostream>

class EditorWindow
:	public Fl_Double_Window
{	EditorWindow(const EditorWindow&) = delete;
	void operator=(const EditorWindow&) = delete;
	static void save_cb();
	static void saveas_cb();
	static void find2_cb(Fl_Widget *, void *);
	static void replall_cb(Fl_Widget *, void *);
	static void replace2_cb(Fl_Widget *, void *);
	static void replcan_cb(Fl_Widget *, void *);
public:
	EditorWindow(int w, int h, const char *t);
	~EditorWindow();
	Fl_Window* replace_dlg;
	Fl_Input* replace_find;
	Fl_Input* replace_with;
	Fl_Button* replace_all;
	Fl_Return_Button* replace_next;
	Fl_Button* replace_cancel;
#ifdef DEV_TEST
	Fl_Button* plus;		/* increase width */
	Fl_Button* minus;		/* decrease width */
	Fl_Button* vscroll;	/* toggle vert. scrollbar left/right */
	Fl_Button* hscroll;	/* toggle hor. scrollbar top/bottom */
	Fl_Button* lnum;		/* toggle line number display */
	Fl_Button* wrap;		/* toggle wrap mode */
#endif /* DEV_TEST */
	int wrap_mode;
	int	line_numbers;
	Fl_Text_Editor* editor;
	char search[256];
public:
	~EditorWindow()
	{	delete replace_dlg;
	}
	EditorWindow()
	{	changed = 0;
		filename[0] = 0;
		title[0] = 0;
		textbuf = 0;
		line_num_width = 75;
	}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	int changed;
	char filename[FL_PATH_MAX];
	char title[FL_PATH_MAX];
	Fl_Text_Buffer* textbuf;
	int line_num_width;
};

inline
std::ostream& operator<<(std::ostream& os,const EditorWindow& editorWindow)
{	return editorWindow.Print(os);
}

inline
std::istream& operator>>(std::istream& is,EditorWindow& editorWindow)
{	return editorWindow.Input(is);
}

#endif


