// EditorWindow.cpp
// Created by Robin Rowe 2020-05-11
// License MIT open source

#include "EditorWindow.h"
using namespace std;

ostream& EditorWindow::Print(ostream& os) const
{	// to-do
	return os << "EditorWindow";
} 

istream& EditorWindow::Input(std::istream& is) 
{	// to-do
	return is;
}

EditorWindow::EditorWindow(int w, int h, const char *t) :
	Fl_Double_Window(w, h, t)
{
	replace_dlg = new Fl_Window(300, 105, "Replace");
	replace_find = new Fl_Input(80, 10, 210, 25, "Find:");
	replace_find->align(FL_ALIGN_LEFT);
	replace_with = new Fl_Input(80, 40, 210, 25, "Replace:");
	replace_with->align(FL_ALIGN_LEFT);
	replace_all = new Fl_Button(10, 70, 90, 25, "Replace All");
	replace_all->callback((Fl_Callback *) replall_cb, this);
	replace_next = new Fl_Return_Button(105, 70, 120, 25, "Replace Next");
	replace_next->callback((Fl_Callback *) replace2_cb, this);
	replace_cancel = new Fl_Button(230, 70, 60, 25, "Cancel");
	replace_cancel->callback((Fl_Callback *) replcan_cb, this);
	replace_dlg->end();
	replace_dlg->set_non_modal();
	editor = 0;
	*search = (char) 0;
	wrap_mode = 0;
	line_numbers = 0;
}

#ifdef DEV_TEST

void resize_cb(Fl_Widget *b, void *v)
{	Fl_Window	*w = b->window();
	int			dw = (int) (long) v;
	const int	fac = 16;	/* factor */
	const int	num = 1;	/* loop count */
	dw *= fac;
	for(int i = 0; i < num; i++)
	{	w->resize(w->x(), w->y(), w->w() + dw, w->h());
	}
}

void scroll_cb(Fl_Widget *b, void *v)
{	EditorWindow	*ew = (EditorWindow *) b->parent();
	Fl_Text_Editor	*ed = ew->editor;
	int				n = (int) (long) v;
	int				align = ed->scrollbar_align();
	switch(n)
	{
		case 1: /* vscroll */
			if(align & FL_ALIGN_LEFT)
			{
				align &= ~FL_ALIGN_LEFT;
				align |= FL_ALIGN_RIGHT;
			}
			else
			{
				align &= ~FL_ALIGN_RIGHT;
				align |= FL_ALIGN_LEFT;
			}
			break;

		case 2: /* hscroll */
			if(align & FL_ALIGN_TOP)
			{
				align &= ~FL_ALIGN_TOP;
				align |= FL_ALIGN_BOTTOM;
			}
			else
			{
				align &= ~FL_ALIGN_BOTTOM;
				align |= FL_ALIGN_TOP;
			}
			break;
		default:
			break;
	}
	ed->scrollbar_align(align);
	ed->resize(ed->x(), ed->y(), ed->w(), ed->h());
	ed->redraw();
}

void wrap_cb(Fl_Widget *w, void *v)
{	EditorWindow	*ew = (EditorWindow *) v;
	Fl_Text_Editor	*ed = (Fl_Text_Editor *) ew->editor;
	ew->wrap_mode = 1 - ew->wrap_mode;
	if(ew->wrap_mode)
	{
		ed->wrap_mode(Fl_Text_Display::WRAP_AT_BOUNDS, 0);
	}
	else
	{
		ed->wrap_mode(Fl_Text_Display::WRAP_NONE, 0);
	}
	ew->redraw();
}

void lnum_cb(Fl_Widget *w, void *v)
{	EditorWindow	*ew = (EditorWindow *) v;
	Fl_Text_Editor	*ed = (Fl_Text_Editor *) ew->editor;
	ew->line_numbers = 1 - ew->line_numbers;
	if(ew->line_numbers)
	{
		ed->linenumber_width(line_num_width);	/* enable */
		ed->linenumber_size(ed->textsize());
	}
	else
	{
		ed->linenumber_width(0);	/* disable */
	}

	ew->redraw();
}
#endif /* DEV_TEST */

int check_save(void)
{	if(!changed)
	{
		return 1;
	}
	int r = fl_choice
		(
			"The current file has not been saved.\n""Would you like to save it now?",
			"Cancel",
			"Save",
			"Don't Save"
		);
	if(r == 1)
	{
		save_cb();	/* Save the file... */
		return !changed;
	}
	return(r == 2) ? 1 : 0;
}

int loading = 0;

void load_file(const char *newfile, int ipos)
{	loading = 1;
	int insert = (ipos != -1);
	changed = insert;
	if(!insert)
	{
		strcpy(filename, "");
	}
	int r;
	if(!insert)
	{
		r = textbuf->loadfile(newfile);
	}
	else
	{
		r = textbuf->insertfile(newfile, ipos);
	}
	changed = changed || textbuf->input_file_was_transcoded;
	if(r)
	{
		fl_alert("Error reading from file \'%s\':\n%s.", newfile, strerror(errno));
	}
	else if(!insert)
	{
		strcpy(filename, newfile);
	}
	loading = 0;
	textbuf->call_modify_callbacks();
}

void save_file(const char *newfile)
{	if(textbuf->savefile(newfile))
	{
		fl_alert("Error writing to file \'%s\':\n%s.", newfile, strerror(errno));
	}
	else
	{
		strcpy(filename, newfile);
	}
	changed = 0;
	textbuf->call_modify_callbacks();
}

void copy_cb(Fl_Widget *, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	Fl_Text_Editor::kf_copy(0, e->editor);
}

void cut_cb(Fl_Widget *, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	Fl_Text_Editor::kf_cut(0, e->editor);
}

void delete_cb(Fl_Widget *, void *)
{	textbuf->remove_selection();
}

void linenumbers_cb(Fl_Widget *w, void *v)
{	EditorWindow		*e = (EditorWindow *) v;
	Fl_Menu_Bar			*m = (Fl_Menu_Bar *) w;
	const Fl_Menu_Item	*i = m->mvalue();
	if(i->value())
	{
		e->editor->linenumber_width(line_num_width);	/* enable */
		e->editor->linenumber_size(e->editor->textsize());
	}
	else
	{
		e->editor->linenumber_width(0); /* disable */
	}
	e->line_numbers = (i->value() ? 1 : 0);
	e->redraw();
}

void wordwrap_cb(Fl_Widget *w, void *v)
{	EditorWindow		*e = (EditorWindow *) v;
	Fl_Menu_Bar			*m = (Fl_Menu_Bar *) w;
	const Fl_Menu_Item	*i = m->mvalue();
	if(i->value())
	{
		e->editor->wrap_mode(Fl_Text_Display::WRAP_AT_BOUNDS, 0);
	}
	else
	{
		e->editor->wrap_mode(Fl_Text_Display::WRAP_NONE, 0);
	}
	e->wrap_mode = (i->value() ? 1 : 0);
	e->redraw();
}

void find_cb(Fl_Widget *w, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	const char		*val;
	val = fl_input("Search String:", e->search);
	if(val != NULL)
	{	/* User entered a string - go find it! */
		strcpy(e->search, val);
		find2_cb(w, v);
	}
}

void find2_cb(Fl_Widget *w, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	if(e->search[0] == '\0')
	{	/* Search string is
		 * blank;
		 * get a new one... */
		find_cb(w, v);
		return;
	}
	int pos = e->editor->insert_position();
	int found = textbuf->search_forward(pos, e->search, &pos);
	if(found)
	{	/* Found a
		 * match;
		 * select and update the position... */
		textbuf->select(pos, pos + strlen(e->search));
		e->editor->insert_position(pos + strlen(e->search));
		e->editor->show_insert_position();
	}
	else
	{
		fl_alert("No occurrences of \'%s\' found!", e->search);
	}
}

void set_title(Fl_Window *w)
{	if(filename[0] == '\0')
	{
		strcpy(title, "Untitled");
	}
	else
	{
		char	*slash;
		slash = strrchr(filename, '/');
#ifdef _WIN32
		if(slash == NULL)
		{
			slash = strrchr(filename, '\\');
		}
#endif
		if(slash != NULL)
		{
			strcpy(title, slash + 1);
		}
		else
		{
			strcpy(title, filename);
		}
	}
	if(changed)
	{
		strcat(title, " (modified)");
	}

	w->label(title);
}

void changed_cb(int, int nInserted, int nDeleted, int, const char *, void *v)
{	if((nInserted || nDeleted) && !loading)
	{
		changed = 1;
	}
	EditorWindow	*w = (EditorWindow *) v;
	set_title(w);
	if(loading)
	{
		w->editor->show_insert_position();
	}
}

void new_cb(Fl_Widget *, void *)
{
	if(!check_save())
	{
		return;
	}
	filename[0] = '\0';
	textbuf->select(0, textbuf->length());
	textbuf->remove_selection();
	changed = 0;
	textbuf->call_modify_callbacks();
}

void open_cb(Fl_Widget *, void *)
{	if(!check_save())
	{
		return;
	}
	Fl_Native_File_Chooser	fnfc;
	fnfc.title("Open file");
	fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
	if(fnfc.show())
	{
		return;
	}

	load_file(fnfc.filename(), -1);
}

void insert_cb(Fl_Widget *, void *v)
{
	Fl_Native_File_Chooser	fnfc;
	fnfc.title("Insert file");
	fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
	if(fnfc.show())
	{
		return;
	}
	EditorWindow	*w = (EditorWindow *) v;
	load_file(fnfc.filename(), w->editor->insert_position());
}

void paste_cb(Fl_Widget *, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	Fl_Text_Editor::kf_paste(0, e->editor);
}

int num_windows = 0;

void close_cb(Fl_Widget *, void *v)
{	EditorWindow	*w = (EditorWindow *) v;
	if(num_windows == 1)
	{
		if(!check_save())
		{
			return;
		}
	}
	w->hide();
	w->editor->buffer(0);
	textbuf->remove_modify_callback(style_update, w->editor);
	textbuf->remove_modify_callback(changed_cb, w);
	Fl::delete_widget(w);
	num_windows--;
	if(!num_windows)
	{
		exit(0);
	}
}

void quit_cb(Fl_Widget *, void *)
{
	if(changed && !check_save())
	{
		return;
	}
	exit(0);
}

void replace_cb(Fl_Widget *, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	e->replace_dlg->show();
}

void replace2_cb(Fl_Widget *, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	const char		*find = e->replace_find->value();
	const char		*replace = e->replace_with->value();
	if(find[0] == '\0')
	{	/* Search string is
		 * blank;
		 * get a new one... */
		e->replace_dlg->show();
		return;
	}
	e->replace_dlg->hide();
	int pos = e->editor->insert_position();
	int found = textbuf->search_forward(pos, find, &pos);
	if(found)
	{	/* Found a
		 * match;
		 * update the position and replace text... */
		textbuf->select(pos, pos + strlen(find));
		textbuf->remove_selection();
		textbuf->insert(pos, replace);
		textbuf->select(pos, pos + strlen(replace));
		e->editor->insert_position(pos + strlen(replace));
		e->editor->show_insert_position();
	}
	else
	{
		fl_alert("No occurrences of \'%s\' found!", find);
	}
}

void replall_cb(Fl_Widget *, void *v)
{	EditorWindow	*e = (EditorWindow *) v;
	const char		*find = e->replace_find->value();
	const char		*replace = e->replace_with->value();
	find = e->replace_find->value();
	if(find[0] == '\0')
	{		/* Search string is
			 * blank;
			 * get a new one... */
		e->replace_dlg->show();
		return;
	}

	e->replace_dlg->hide();
	e->editor->insert_position(0);
	int times = 0;
	for(int found = 1; found;)
	{
		int pos = e->editor->insert_position();
		found = textbuf->search_forward(pos, find, &pos);
		if(found)
		{	/* Found a
			 * match;
			 * update the position and replace text... */
			textbuf->select(pos, pos + strlen(find));
			textbuf->remove_selection();
			textbuf->insert(pos, replace);
			e->editor->insert_position(pos + strlen(replace));
			e->editor->show_insert_position();
			times++;
		}
	}
	if(times)
	{
		fl_message("Replaced %d occurrences.", times);
	}
	else
	{
		fl_alert("No occurrences of \'%s\' found!", find);
	}
}

void replcan_cb(Fl_Widget *, void *v)
{
	EditorWindow	*e = (EditorWindow *) v;
	e->replace_dlg->hide();
}

void save_cb()
{	if(filename[0] == '\0')
	{	/* No filename - get one! */
		saveas_cb();
		return;
	}
	else
	{
		save_file(filename);
	}
}

void saveas_cb()
{
	Fl_Native_File_Chooser	fnfc;
	fnfc.title("Save File As?");
	fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
	if(fnfc.show())
	{
		return;
	}
	save_file(fnfc.filename());
}

Fl_Window* new_view();

void view_cb(Fl_Widget *, void *)
{	Fl_Window	*w = new_view();
	w->show();
}
