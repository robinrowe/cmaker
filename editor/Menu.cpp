// Menu.cpp
// Created by Robin Rowe 2020-05-11
// License MIT open source

#include "Menu.h"
using namespace std;

ostream& Menu::Print(ostream& os) const
{	// to-do
	return os << "Menu";
} 

istream& Menu::Input(std::istream& is) 
{	// to-do
	return is;
}


Fl_Menu_Item	menuitems[] =
{
	{ "&File", 0, 0, 0, FL_SUBMENU },
	{ "&New File", 0, (Fl_Callback *) new_cb },
	{ "&Open File...", FL_COMMAND + 'o', (Fl_Callback *) open_cb },
	{ "&Insert File...", FL_COMMAND + 'i', (Fl_Callback *) insert_cb, 0, FL_MENU_DIVIDER },
	{ "&Save File", FL_COMMAND + 's', (Fl_Callback *) save_cb },
	{ "Save File &As...", FL_COMMAND + FL_SHIFT + 's', (Fl_Callback *) saveas_cb, 0, FL_MENU_DIVIDER },
	{ "New &View", FL_ALT
#ifdef __APPLE__
		+ FL_COMMAND
#endif
		+'v', (Fl_Callback *) view_cb, 0 },
	{ "&Close View", FL_COMMAND + 'w', (Fl_Callback *) close_cb, 0, FL_MENU_DIVIDER },
	{ "E&xit", FL_COMMAND + 'q', (Fl_Callback *) quit_cb, 0 },
	{ 0 },
	{ "&Edit", 0, 0, 0, FL_SUBMENU },
	{ "Cu&t", FL_COMMAND + 'x', (Fl_Callback *) cut_cb },
	{ "&Copy", FL_COMMAND + 'c', (Fl_Callback *) copy_cb },
	{ "&Paste", FL_COMMAND + 'v', (Fl_Callback *) paste_cb },
	{ "&Delete", 0, (Fl_Callback *) delete_cb },
	{ "Preferences", 0, 0, 0, FL_SUBMENU },
	{ "Line Numbers", FL_COMMAND + 'l', (Fl_Callback *) linenumbers_cb, 0, FL_MENU_TOGGLE },
	{ "Word Wrap", 0, (Fl_Callback *) wordwrap_cb, 0, FL_MENU_TOGGLE },
	{ 0 },
	{ 0 },
	{ "&Search", 0, 0, 0, FL_SUBMENU },
	{ "&Find...", FL_COMMAND + 'f', (Fl_Callback *) find_cb },
	{ "F&ind Again", FL_COMMAND + 'g', find2_cb },
	{ "&Replace...", FL_COMMAND + 'r', replace_cb },
	{ "Re&place Again", FL_COMMAND + 't', replace2_cb },
	{ 0 },
	{ 0 }
};

Fl_Window *new_view()
{
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#ifdef DEV_TEST
	EditorWindow	*w = new EditorWindow(660, 500, title);
#else
	EditorWindow	*w = new EditorWindow(660, 400, title);
#endif
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	/* DEV_TEST */
	w->begin();

	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	Fl_Menu_Bar *m = new Fl_Menu_Bar(0, 0, 660, 30);
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	m->copy(menuitems, w);
	w->editor = new Fl_Text_Editor(0, 30, 660, 370);
	w->editor->textfont(FL_COURIER);
	w->editor->textsize(TS);

	/*
	 * w->editor->wrap_mode(Fl_Text_Editor::WRAP_AT_BOUNDS, 250);
	 */
	w->editor->buffer(textbuf);
	w->editor->highlight_data
		(
			stylebuf,
			styletable,
			sizeof(styletable) / sizeof(styletable[0]),
			'A',
			style_unfinished_cb,
			0
		);
#ifdef DEV_TEST
	w->minus = new Fl_Button(60, 410, 120, 30, "&-");
	w->minus->labelsize(20);
	w->minus->labelfont(FL_BOLD);
	w->minus->callback(resize_cb, (void *) (-1));
	w->plus = new Fl_Button(60, 450, 120, 30, "&+");
	w->plus->labelsize(20);
	w->plus->labelfont(FL_BOLD);
	w->plus->callback(resize_cb, (void *) 1);
	w->vscroll = new Fl_Button(220, 410, 120, 30, "&vscroll");
	w->vscroll->labelsize(16);
	w->vscroll->callback(scroll_cb, (void *) 1);
	w->hscroll = new Fl_Button(220, 450, 120, 30, "&hscroll");
	w->hscroll->labelsize(16);
	w->hscroll->callback(scroll_cb, (void *) 2);
	w->lnum = new Fl_Button(380, 410, 120, 30, "&line #");
	w->lnum->labelsize(16);
	w->lnum->callback(lnum_cb, (void *) w);
	w->wrap = new Fl_Button(380, 450, 120, 30, "&wrap");
	w->wrap->labelsize(16);
	w->wrap->callback(wrap_cb, (void *) w);
#endif /* DEV_TEST */
	w->end();
	w->resizable(w->editor);
	w->size_range(300, 200);
	w->callback((Fl_Callback *) close_cb, w);
	textbuf->add_modify_callback(style_update, w->editor);
	textbuf->add_modify_callback(changed_cb, w);
	textbuf->call_modify_callbacks();
	num_windows++;
	return w;
}

void cb(const char *fname)
{
	load_file(fname, -1);
}
