// Menu.h 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#ifndef Menu_h
#define Menu_h

#include <iostream>

class Menu
{	Menu(const Menu&) = delete;
	void operator=(const Menu&) = delete;

public:
	~Menu()
	{}
	Menu()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Menu& menu)
{	return menu.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Menu& menu)
{	return menu.Input(is);
}

#endif
