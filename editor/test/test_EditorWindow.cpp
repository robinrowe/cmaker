// test_EditorWindow.cpp 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#include <iostream>
#include "../EditorWindow.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing EditorWindow" << endl;
	EditorWindow editorWindow;
	if(!editorWindow)
	{	cout << "EditorWindow failed on operator!" << endl;
		return 1;
	}
	cout << editorWindow << endl;
	cout << "EditorWindow Passed!" << endl;
	return 0;
}
