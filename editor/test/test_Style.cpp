// test_Style.cpp 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#include <iostream>
#include "../Style.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Style" << endl;
	Style style;
	if(!style)
	{	cout << "Style failed on operator!" << endl;
		return 1;
	}
	cout << style << endl;
	cout << "Style Passed!" << endl;
	return 0;
}
