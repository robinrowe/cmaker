// test_Menu.cpp 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#include <iostream>
#include "../Menu.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Menu" << endl;
	Menu menu;
	if(!menu)
	{	cout << "Menu failed on operator!" << endl;
		return 1;
	}
	cout << menu << endl;
	cout << "Menu Passed!" << endl;
	return 0;
}
