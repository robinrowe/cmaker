// cmaker.cpp 
// Created by Robin Rowe 2020-05-11
// License MIT open source

#include <iostream>
using namespace std;

void Usage()
{	cout << "Usage: cmaker " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "cmaker starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}
	textbuf = new Fl_Text_Buffer;
	/*
	 * textbuf->transcoding_warning_action = NULL;
	 */
	style_init();
	fl_open_callback(cb);
	Fl_Window	*window = new_view();
	window->show(1, argv);
#ifndef __APPLE__
	if(argc > 1)
	{
		load_file(argv[1], -1);
	}
#endif
	Fl::run();
	cout << "cmaker done!" << endl;
	return ok;
}
