// ProgramMaker.h 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#ifndef ProgramMaker_h
#define ProgramMaker_h

#include <iostream>

class ProgramMaker
{	ProgramMaker(const ProgramMaker&) = delete;
	void operator=(const ProgramMaker&) = delete;

public:
	~ProgramMaker()
	{}
	ProgramMaker()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	bool Run(int argc,char* argv[])
	{	return false;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const ProgramMaker& programMaker)
{	return programMaker.Print(os);
}


inline
std::istream& operator>>(std::istream& is,ProgramMaker& programMaker)
{	return programMaker.Input(is);
}

#endif
