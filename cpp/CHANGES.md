#CHANGES.md

Robin Rowe 2023-02-06

cmaker: project CppMaker

## To-Do

## Done

cmaker: project CppMaker
cmaker: program cppmaker
cmaker: program kloc
cmaker: class ClassMaker
cmaker: class ProgramMaker
cmaker: class ProjectMaker
cmaker: class Parser
cmaker: class Boilerplate
cmaker: class Create
cmaker: class Kloc
cmaker: class AddUnistd
cmaker: class AddFLTK
cmaker: class AddGtk1
cmaker: class AddLibrary
cmaker: class License
cmaker: class Author
cmaker: class FileReader
cmaker: class Directory
