// ProjectMaker.h 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#ifndef ProjectMaker_h
#define ProjectMaker_h

#include <iostream>

class ProjectMaker
{	ProjectMaker(const ProjectMaker&) = delete;
	void operator=(const ProjectMaker&) = delete;

public:
	~ProjectMaker()
	{}
	ProjectMaker()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	bool Run(int argc,char* argv[])
	{	return false;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const ProjectMaker& projectMaker)
{	return projectMaker.Print(os);
}


inline
std::istream& operator>>(std::istream& is,ProjectMaker& projectMaker)
{	return projectMaker.Input(is);
}

#endif
