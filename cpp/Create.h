// Create.h 
// Created by Robin Rowe 2023-08-25
// License MIT open source

#ifndef Create_h
#define Create_h

#include <iostream>
#include <string>
#include <filesystem>
#include <algorithm>

class Create
{	Create(const Create&) = delete;
	void operator=(const Create&) = delete;
	static void Clean(const std::filesystem::path& path);
//	static void SetProjectName(std::string& project_name);
	bool CreateCmakeList(const std::string& project_name,const std::filesystem::path& path,const std::string& cmakelists_text) const;
	bool AddSubdir(const std::filesystem::path& path,const std::filesystem::path& subdir) const;
	bool AddDirSources(const std::filesystem::path& path) const;
	bool AddSubdirs(const std::filesystem::path& path) const;
	static void RemoveCmakerFiles(const std::filesystem::path& path);
	std::string GetDateTime() const;
	std::string GetDate() const;
	static bool IsCppFile(const std::filesystem::path& ext);
	bool IsIgnoreDir(const std::filesystem::path& dir) const;
	size_t GetPathOffset() const
	{	return projectdir.string().size() - projectdir.filename().string().size();
	}
	static void FlipSlashes(std::string& path) 
	{	std::replace(path.begin(),path.end(),'\\','/');
	}
	std::string GetShortPath(std::string path) const
	{	const size_t path_offset = GetPathOffset();
		std::string short_path(path,path_offset);
		FlipSlashes(short_path);
		return short_path;
	}
	bool SetAuthor(const char* filename);
	bool SetLicense(const char* filename);
	std::string ReplaceTopText(const std::filesystem::path& path) const;
	std::filesystem::path homedir;
	std::filesystem::path projectdir;
	std::string author;
	std::string license;
	std::string top_cmakelists;
	std::string subdir_cmakelists;
	bool is_good;
public:
	~Create()
	{}
	Create(const std::filesystem::path& homedir,const std::filesystem::path& projectdir);
	bool operator!() const
	{	return !is_good;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	bool Build(const std::string& project_name);
};

inline
std::ostream& operator<<(std::ostream& os,const Create& create)
{	return create.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Create& create)
{	return create.Input(is);
}

#endif

/*
 	set<fs::path> sorted_by_name;
	for (auto &entry : fs::directory_iterator(path))
    {	sorted_by_name.insert(entry.path());
	}

    std::string ext(".txt");
    for (auto& p : fs::recursive_directory_iterator(constructed_path_str_dbg))
    {
        if (p.path().extension() == ext)  

	for (auto &filename : sorted_by_name)
    {	cout << filename << endl;
	}
*/