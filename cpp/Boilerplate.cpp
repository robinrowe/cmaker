// Boilerplate.cpp
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include "Boilerplate.h"
using namespace std;

ostream& Boilerplate::Print(ostream& os) const
{	// to-do
	return os << "Boilerplate";
} 

istream& Boilerplate::Input(std::istream& is) 
{	// to-do
	return is;
}
