// test_Boilerplate.cpp 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include <iostream>
#include "../Boilerplate.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Boilerplate" << endl;
	Boilerplate boilerplate;
	if(!boilerplate)
	{	cout << "Boilerplate failed on operator!" << endl;
		return 1;
	}
	cout << boilerplate << endl;
	cout << "Boilerplate Passed!" << endl;
	return 0;
}
