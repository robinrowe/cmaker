// test_Kloc.cpp 
// Created by Robin Rowe 2023-08-25
// License MIT open source

#include <iostream>
#include "../Kloc.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Kloc" << endl;
	Kloc kloc;
	if(!kloc)
	{	cout << "Kloc failed on operator!" << endl;
		return 1;
	}
	cout << kloc << endl;
	cout << "Kloc Passed!" << endl;
	return 0;
}
