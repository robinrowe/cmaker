// test_Directory.cpp 
// Created by Robin Rowe 2023-08-26
// License MIT open source

#include <iostream>
#include "../Directory.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Directory" << endl;
	Directory directory(argv[0],"build");
	if(!directory)
	{	cout << "Directory failed on operator!" << endl;
		return 1;
	}
	cout << directory << endl;
	cout << "Directory Passed!" << endl;
	return 0;
}
