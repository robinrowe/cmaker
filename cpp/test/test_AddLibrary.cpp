// test_AddLibrary.cpp 
// Created by Robin Rowe 2023-08-25
// License MIT open source

#include <iostream>
#include "../AddLibrary.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing AddLibrary" << endl;
	AddLibrary addLibrary;
	if(!addLibrary)
	{	cout << "AddLibrary failed on operator!" << endl;
		return 1;
	}
	cout << addLibrary << endl;
	cout << "AddLibrary Passed!" << endl;
	return 0;
}
