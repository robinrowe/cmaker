// test_Create.cpp 
// Created by Robin Rowe 2023-08-25
// License MIT open source

#include <iostream>
#include "../Create.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Create" << endl;
	Create create(argv[0],"HelloWorld");
	if(!create)
	{	cout << "Create failed on operator!" << endl;
		return 1;
	}
	cout << create << endl;
	cout << "Create Passed!" << endl;
	return 0;
}
