// test_FileReader.cpp 
// Created by Robin Rowe 2023-08-26
// License MIT open source

#include <iostream>
#include <fstream>
#include "../FileReader.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing FileReader" << endl;
	const char* filename = "test_file_reader.txt";
	ofstream os(filename);
	os << "Hello World" << endl;
	os.close();
	FileReader fileReader(filename);
	if(!fileReader)
	{	cout << "FileReader failed on operator!" << endl;
		return 1;
	}
	cout << fileReader << endl;
	cout << "FileReader Passed!" << endl;
	return 0;
}
