// test_ProjectMaker.cpp 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include <iostream>
#include "../ProjectMaker.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing ProjectMaker" << endl;
	ProjectMaker projectMaker;
	if(!projectMaker)
	{	cout << "ProjectMaker failed on operator!" << endl;
		return 1;
	}
	cout << projectMaker << endl;
	cout << "ProjectMaker Passed!" << endl;
	return 0;
}
