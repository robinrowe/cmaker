// test_ClassMaker.cpp 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include <iostream>
#include "../ClassMaker.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing ClassMaker" << endl;
	ClassMaker classMaker;
	if(!classMaker)
	{	cout << "ClassMaker failed on operator!" << endl;
		return 1;
	}
	cout << classMaker << endl;
	cout << "ClassMaker Passed!" << endl;
	return 0;
}
