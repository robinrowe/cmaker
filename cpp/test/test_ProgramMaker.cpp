// test_ProgramMaker.cpp 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include <iostream>
#include "../ProgramMaker.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing ProgramMaker" << endl;
	ProgramMaker programMaker;
	if(!programMaker)
	{	cout << "ProgramMaker failed on operator!" << endl;
		return 1;
	}
	cout << programMaker << endl;
	cout << "ProgramMaker Passed!" << endl;
	return 0;
}
