// Kloc.h 
// Created by Robin Rowe 2023-08-25
// License MIT open source

#ifndef Kloc_h
#define Kloc_h

#include <iostream>

class Kloc
{	Kloc(const Kloc&) = delete;
	void operator=(const Kloc&) = delete;

public:
	~Kloc()
	{}
	Kloc()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	bool Run(int argc,char* argv[])
	{	return false;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const Kloc& kloc)
{	return kloc.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Kloc& kloc)
{	return kloc.Input(is);
}

#endif
