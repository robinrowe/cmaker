// AddLibrary.h 
// Created by Robin Rowe 2023-08-25
// License MIT open source

#ifndef AddLibrary_h
#define AddLibrary_h

#include <iostream>

class AddLibrary
{	AddLibrary(const AddLibrary&) = delete;
	void operator=(const AddLibrary&) = delete;

public:
	~AddLibrary()
	{}
	AddLibrary()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	bool Run(int argc,char* argv[])
	{	return false;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const AddLibrary& addLibrary)
{	return addLibrary.Print(os);
}


inline
std::istream& operator>>(std::istream& is,AddLibrary& addLibrary)
{	return addLibrary.Input(is);
}

#endif
