// Boilerplate.h 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#ifndef Boilerplate_h
#define Boilerplate_h

#include <iostream>

class Boilerplate
{	Boilerplate(const Boilerplate&) = delete;
	void operator=(const Boilerplate&) = delete;

public:
	~Boilerplate()
	{}
	Boilerplate()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Boilerplate& boilerplate)
{	return boilerplate.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Boilerplate& boilerplate)
{	return boilerplate.Input(is);
}

#endif
