// cppmaker.cpp 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include <iostream>
#include "Parser.h"
#include "ClassMaker.h"
#include "Create.h"
#include "Kloc.h"
#include "ProgramMaker.h"
#include "ProjectMaker.h"
#include "AddLibrary.h"
#include "Directory.h"
using namespace std;

#define TESTING_CREATE

const char* CMAKER_DATA_DIR = "cmaker_data";

void Usage(Parser::Action action)
{	cout << "Usage: cmaker ACTION name(s)..." << endl
		<< "where ACTION may be: class, program, project followed by the name(s) to create" << endl
		<< "or ACTION create, followed by the name of the project to create recursive build system" << endl
		<< "or ACTION kloc, to count lines of code" << endl
		<< "or ACTION library, with name unistd, fltk or gtk1" << endl;
	switch(action)
	{	default:
		case Parser::cmaker_unknown:
			cout<< "Error: unknown action" << endl;
			return;
		case Parser::cmaker_class:
			cout<< "Use: unknown action" << endl;
			return;
		case Parser::cmaker_create:
			cout<< "Use: create PROJECT_NAME" << endl;
			return;
		case Parser::cmaker_kloc:
			cout<< "Use: unknown action" << endl;
			return;
		case Parser::cmaker_program:
			cout<< "Use: unknown action" << endl;
			return;
		case Parser::cmaker_project:
			cout<< "Use: unknown action" << endl;
			return;
		case Parser::cmaker_library:
			cout<< "Use: unknown action" << endl;
			return;
	}
}

enum
{	ok,
	invalid_args,
	invalid_action,
	failed
};

#define STOP(x)	cout << #x; return x

int main(int argc,char* argv[])
{	cout << "cmaker starting..." << endl;
	if(argc < 1)
	{	Usage(Parser::Action::cmaker_unknown);
		STOP(invalid_args);
	}
	Parser parser;
	const char* action = argv[1];
#ifdef TESTING_CREATE
	action = "create";
#endif
	if(!parser.Set(action))
	{	Usage(parser.action);
		STOP(invalid_action);
	}
	bool isOk = false;
	switch(parser.action)
	{	default:
		{	cout << "Don't understand: " << argv[1] << endl;
			Usage(parser.action);
			STOP(invalid_action);
		}
		case Parser::cmaker_class:
		{	ClassMaker classMaker;
			isOk = classMaker.Run(argc,argv);
			break;
		}
		case Parser::cmaker_create:
		{	filesystem::path projectdir;
			filesystem::path homedir;
			string project_name;
#ifdef TESTING_CREATE
			action = "create";
			projectdir = string("/code/github/tinycc");
			projectdir.make_preferred();
			const char* argv0  = "/code/gitlab/cmaker/cpp/build/x64/Debug/cmaker.exe";
			Directory dir(argv0,CMAKER_DATA_DIR);
			homedir = dir.homedir;
			project_name = "TinyCC";
#else
			// "cmaker.exe","create","TestProject"
			if(argc<3)
			{	Usage(parser.action);
				return(invalid_action);
			}
			Directory dir(argv[0]);
			projectdir = dir.cwd;
			homedir = dir.zero_path;
			project_name = argv[2];
#endif
			Create create(homedir,projectdir);			
			isOk = create.Build(project_name);
			break;
		}
		case Parser::cmaker_kloc:
		{	Kloc kloc;
			isOk = kloc.Run(argc,argv);
			break;
		}
		case Parser::cmaker_program:
		{	ProgramMaker programMaker;
			isOk = programMaker.Run(argc,argv);
			break;
		}
		case Parser::cmaker_project:
		{	ProjectMaker projectMaker;
			isOk = projectMaker.Run(argc,argv);
			break;
		}
		case Parser::cmaker_library:
		{	AddLibrary addLibrary;
			isOk = addLibrary.Run(argc,argv);
			break;
	}	}
	if(!isOk)
	{	STOP(failed);
	}
	STOP(ok);
}
