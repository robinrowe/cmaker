// Parser.cpp
// Created by Robin Rowe 2023-02-06
// License MIT open source

#include <string>
#include <algorithm>
#include "Parser.h"
using namespace std;

ostream& Parser::Print(ostream& os) const
{	// to-do
	return os << "Parser";
} 

istream& Parser::Input(std::istream& is) 
{	// to-do
	return is;
}

bool Parser::Set(const char* arg)
{	action = cmaker_unknown;
	const string s(arg);
	for_each(s.begin(),s.end(),tolower);
	int i = 1;
	for(int i = 1;i < cmaker_ends;i++)
	{	if(s == actions[i])
		{	action = (Action) i;
			return true;
	}	}
	return false;
}

int Parser::ReplaceText(string& text,const string& before,const string& after)
{   if(!text[0])
    {   return 0;
    }
    unsigned count = 0;
    size_t i = text.find(before);
	while(i != std::string::npos)
	{	count++;
        text.erase(i,before.size());
        text.insert(i,after);
        i = text.find(before);
    }
    return count;
}
