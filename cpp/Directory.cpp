// Directory.cpp
// Created by Robin Rowe 2023-08-26
// License MIT open source

#include <direct.h>
#include "Directory.h"
using namespace std;

ostream& Directory::Print(ostream& os) const
{	// to-do
	return os << "Directory";
} 

istream& Directory::Input(std::istream& is) 
{	// to-do
	return is;
}

std::filesystem::path Directory::GetDataFile(const char* subdir,const char* filename) const
{	std::filesystem::path datafile(homedir);
	std::filesystem::path basename = datafile.filename();
	if(basename == "Debug" || basename == "Release")
	{	datafile = datafile.parent_path();
	}
	datafile.append("/");
	datafile.append(subdir);
	datafile.append("/");
	datafile.append(filename);
	return (datafile);
}

#pragma warning(disable:4996)

bool Directory::SetHomedir(const char* path,const char* datadir)
{	std::filesystem::path basename(path);
	basename.make_preferred();
	while(basename.string().size())
	{	homedir = basename;
		homedir.append(datadir);
		if(filesystem::is_directory(homedir))
		{	return true;
		}
		basename = basename.parent_path();
	}
	return false;
}

void Directory::SetWorkdir()
{	const char* dir = getenv("CMAKER_PATH");
	if(dir)
	{	workdir = dir;
	}
	else
	{	workdir = std::filesystem::current_path().string();
	}
	workdir.make_preferred();
}

