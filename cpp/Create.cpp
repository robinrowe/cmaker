// Create.cpp
// Created by Robin Rowe 2023-08-25
// License MIT open source

#include <stdio.h>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <format>
#include <set>
#include "Create.h"
#include "FileReader.h"
#include "Directory.h"
#include "Parser.h"
using namespace std;

//#define SHOW_CLEANING
//#define SHOW_IGNORING
#define SHOW_VERBOSE

#ifdef SHOW_VERBOSE
#define VERBOSE(msg) cout << "VERBOSE: " << msg << " << "
#else
//#define VERBOSE(msg) // cout
#endif

const char* AUTHOR_FILE = "AUTHOR";
const char* LICENSE_FILE = "LICENSE";
const char* CMAKE_FILE = "CMakeLists.txt";
const char* CMAKE_SOURCES_FILE = "sources.cmake";

const char* CMAKER_PATH = "CMAKER_PATH";
const char* CMAKER_PROJECT = "CMAKER_PROJECT";
const char* CMAKER_DATE = "CMAKER_DATE";
const char* CMAKER_AUTHOR = "CMAKER_AUTHOR";
const char* CMAKER_LICENSE = "CMAKER_LICENSE";
const char* CMAKER_TOP_CMAKELISTS = "cmaker_create_cmakelists.txt";
const char* CMAKER_SUBDIR_CMAKELISTS = "cmaker_create_subdir.txt";

ostream& Create::Print(ostream& os) const
{	// to-do
//	const char (&p)[6] = "world";
	return os << "Create";
} 

istream& Create::Input(std::istream& is) 
{	// to-do
	return is;
}

Create::Create(const std::filesystem::path& homedir,const std::filesystem::path& projectdir)
:	homedir(homedir)
,	projectdir(projectdir)
,	is_good(false)
{	filesystem::path cmakelists(homedir);
	cmakelists.append(CMAKER_TOP_CMAKELISTS);
	{	FileReader reader(cmakelists.string().c_str());
		if(!reader)
		{	return;
		}
		top_cmakelists = move(reader.text);
	}
	cmakelists.replace_filename(CMAKER_SUBDIR_CMAKELISTS);
	{	FileReader reader(cmakelists.string().c_str());
		if(!reader)
		{	return;
		}
		subdir_cmakelists = move(reader.text);
	}
	is_good = true;
}

string Create::GetDateTime() const
{	auto const time = chrono::current_zone()->to_local(chrono::system_clock::now());
    return format("{:%Y-%m-%d %X}",time);
}

string Create::GetDate() const
{	auto const time = chrono::current_zone()->to_local(chrono::system_clock::now());
    return format("{:%Y-%m-%d}",time);
}

bool Create::SetAuthor(const char* filename)
{	FileReader author_file(filename);
	if(!author_file)
	{	cout << "In bash, set your name using: % echo \"Your Name\" > AUTHOR" << endl;
		return false;
	}
	author = move(author_file.text);
	return true;
}

bool Create::SetLicense(const char* filename)
{	FileReader license_file(filename);
	if(!license_file)
	{	return false;
	}
	license = move(license_file.text);
	return true;
}

bool Create::Build(const string& project_name) 
{	std::filesystem::path filename(projectdir);
	filename.make_preferred();
	filename.append(AUTHOR_FILE);
	if(!SetAuthor(filename.string().c_str()))
	{	cout << "In bash, set your name using: % echo \"Your Name\" > AUTHOR" << endl;
		return false;
	}
	filename.replace_filename(LICENSE_FILE);
	if(!SetLicense(filename.string().c_str()))
	{	cout << "In bash, set your license name, such as: % echo \"MIT Open Source\" > LICENSE" << endl;
		return false;
	}
	Clean(projectdir);
	RemoveCmakerFiles(projectdir);
	filesystem::path cmake_file(projectdir);
	cmake_file.append(CMAKE_FILE);
	CreateCmakeList(project_name,projectdir,cmake_file.string());
	AddSubdirs(projectdir);
	cout << "done!" << endl
		<< "You may next: 'mkdir build; cd build; cmake ..'" << endl;
	return true;
}

void Create::RemoveCmakerFiles(const filesystem::path& path)
{	string s(path.string());
	FlipSlashes(s);
#ifdef SHOW_CLEANING
	cout << "Cleaning: " << s << endl;
#endif
	filesystem::path rm(path);
	rm.append(CMAKE_FILE); 
	::remove(rm.string().c_str());
	rm.replace_filename(CMAKE_SOURCES_FILE);
	::remove(rm.string().c_str());
}

void Create::Clean(const std::filesystem::path& path)
{	for (const auto& entry : filesystem::directory_iterator(path))
	{	if(!entry.is_directory())
		{	continue;
		}
		const filesystem::path& dir(entry.path());
		const filesystem::path dirname = dir.filename();
		if(dirname == ".git" || dirname == "build")
		{	continue;
		}
		RemoveCmakerFiles(dir);
		if(dir == path)
		{	continue;
		}
	    Clean(entry.path());
	}
}

string Create::ReplaceTopText(const filesystem::path& path) const
{	string s = top_cmakelists;
	const string short_path = GetShortPath(path.string());
	string date = GetDate();
	Parser::ReplaceText(s,CMAKER_DATE,date.c_str());
	Parser::ReplaceText(s,CMAKER_PATH,short_path.c_str());
	Parser::ReplaceText(s,CMAKER_PROJECT,path.filename().string().c_str());
	Parser::ReplaceText(s,CMAKER_AUTHOR,author.c_str());
	Parser::ReplaceText(s,CMAKER_LICENSE,license.c_str());
	return s;
}

bool Create::CreateCmakeList(const std::string& project_name,const std::filesystem::path& path,const std::string& cmakelists_text) const
{	string project(project_name);
#ifdef SHOW_VERBOSE
	cout << "VERBOSE: CreateCmakeList(" << project_name << "," << path << ",cmakelists_text)" << endl;
#endif
	replace(project.begin(),project.end(),'.','_');
	string cmakelists = ReplaceTopText(path);
	filesystem::path cmake_file(path);
	cmake_file.append(CMAKE_FILE);
	ofstream os(cmake_file);
	os << cmakelists;
	if(os.bad())
	{	return false;
	}
	return true;
}

bool Create::AddSubdir(const filesystem::path& path,const filesystem::path& subdir) const
{	if(path == projectdir)
	{	return false;
	}
	filesystem::path cmakelist = path.parent_path();
	cmakelist.append(CMAKE_FILE);
	ofstream os(cmakelist);
#ifdef SHOW_VERBOSE
	VERBOSE(cmakelist) << ": add_subdirectory(" << subdir << ")" << endl;
#endif
	os << "add_subdirectory(" << subdir << ")" << endl;
	return !os.bad();
}

bool Create::IsCppFile(const filesystem::path& ext) 
{	for(filesystem::path c_ext : {".h",".c",".cc",".cpp",".cxx"})
	{	if(ext == c_ext)
		{	return true;
	}	}
	return false;
}

bool Create::IsIgnoreDir(const filesystem::path& dir) const
{	filesystem::path short_path = GetShortPath(dir.string());
	for(filesystem::path exclude : {".git","build"})
	{	if(short_path.filename() == exclude)
		{	return true;
	}	}
	return false;
}

bool Create::AddDirSources(const filesystem::path& path) const
{	set<filesystem::path> files;
	for (const auto& entry : filesystem::directory_iterator(path))
	{	const auto& ext = entry.path().extension();
		if(!IsCppFile(ext))
		{	continue;
		}
		files.insert(entry.path().filename());
	}
	if(!files.size())
	{	return true;
	}
	filesystem::path cmakelist(path);
	cmakelist.append(CMAKE_FILE);
	ofstream os(cmakelist);
#ifdef SHOW_VERBOSE
	VERBOSE(cmakelist) << "file(STRINGS sources.cmake SOURCES)" << endl;
#endif
	os << "file(STRINGS sources.cmake SOURCES)" << endl;
	if(os.bad())
	{	cout << "Error writing to " << cmakelist << endl;
		return false;
	}
	os.close();
	filesystem::path sources(path);
	sources.append("/sources.cmake");
	os.open(sources);
	for (const auto& filename : files)
	{
#ifdef SHOW_VERBOSE
		VERBOSE(sources) << filename << endl;	
#endif
		os << filename << endl;
	}
	if(os.bad())
	{	cout << "Error writing to " << sources << endl;
		return false;
	}
	cout << path << ": added " << files.size() << " source file(s)" << endl;
	return true;
}

bool Create::AddSubdirs(const filesystem::path& path) const
{	if(IsIgnoreDir(path))
	{
#ifdef SHOW_IGNORING
		cout << "Ignoring " << path << endl;
#endif
		return true;;
	}
	if(path != projectdir)
	{	filesystem::path datafile(homedir);
		datafile.append(CMAKER_SUBDIR_CMAKELISTS);
		const string project_name(path.filename().string());
		CreateCmakeList(project_name,path,datafile.string());
		AddSubdir(path,path.filename());
		AddDirSources(path);
	}
	for (const auto& entry : filesystem::directory_iterator(path))
	{	if(entry.is_directory())
		{	AddSubdirs(entry.path());
	}	}
	return true;
}


