// FileReader.cpp
// Created by Robin Rowe 2023-08-26
// License MIT open source

#include <fstream>
#include <sstream>
#include <algorithm>
#include "FileReader.h"
using namespace std;

ostream& FileReader::Print(ostream& os) const
{	return os << text;
} 

istream& FileReader::Input(std::istream& is) 
{   stringstream ss;
    ss << is.rdbuf();
    text = move(ss.str());
    return is;
}

FileReader::FileReader(const char* filename)
:   is_good(false)
{   this->filename = filename;
    ifstream is(filename,ios::in);
    if(!is.is_open())
    {   return;
    }
    Input(is);
    is_good = !is.bad();
}

