// FileReader.h 
// Created by Robin Rowe 2023-08-26
// License MIT open source

#ifndef FileReader_h
#define FileReader_h

#include <iostream>
#include <string>
#include "Parser.h"

class FileReader
{	FileReader(const FileReader&) = delete;
	void operator=(const FileReader&) = delete;
	bool is_good;
public:
	std::string filename;
	std::string text;
	~FileReader()
	{}
	FileReader(const char* filename);
	bool operator!() const
	{	return !is_good;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	int ReplaceText(const char* before,const char* after)
	{	return Parser::ReplaceText(text,before,after);
	}
};

inline
std::ostream& operator<<(std::ostream& os,const FileReader& fileReader)
{	return fileReader.Print(os);
}


inline
std::istream& operator>>(std::istream& is,FileReader& fileReader)
{	return fileReader.Input(is);
}

#endif
