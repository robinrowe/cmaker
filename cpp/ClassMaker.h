// ClassMaker.h 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#ifndef ClassMaker_h
#define ClassMaker_h

#include <iostream>

class ClassMaker
{	ClassMaker(const ClassMaker&) = delete;
	void operator=(const ClassMaker&) = delete;

public:
	~ClassMaker()
	{}
	ClassMaker()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	bool Run(int argc,char* argv[])
	{	return false;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const ClassMaker& classMaker)
{	return classMaker.Print(os);
}


inline
std::istream& operator>>(std::istream& is,ClassMaker& classMaker)
{	return classMaker.Input(is);
}

#endif
