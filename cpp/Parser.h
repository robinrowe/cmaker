// Parser.h 
// Created by Robin Rowe 2023-02-06
// License MIT open source

#ifndef Parser_h
#define Parser_h

#include <iostream>

class Parser
{	Parser(const Parser&) = delete;
	void operator=(const Parser&) = delete;
	const char** actions;
public:
	enum Action
	{	cmaker_unknown,
		cmaker_class,
		cmaker_create,
//		cmaker_examples.sh
		cmaker_kloc,
		cmaker_program,
		cmaker_project,
		cmaker_library,
		cmaker_ends // Always make this last
	};
	Action action;
	~Parser()
	{}
	Parser()
	:	action(cmaker_unknown)
	{	const char* a[] =
		{	"unknown",
			"class",
			"create",
			"kloc",
			"program",
			"project",
			"library"
		};
		actions = a;
	}
	bool Set(const char* arg);
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	static int ReplaceText(std::string& text,const std::string& before,const std::string& after);
};

inline
std::ostream& operator<<(std::ostream& os,const Parser& parser)
{	return parser.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Parser& parser)
{	return parser.Input(is);
}

#endif
