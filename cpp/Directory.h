// Directory.h 
// Created by Robin Rowe 2023-08-26
// License MIT open source

#ifndef Directory_h
#define Directory_h

#include <iostream>
#include <filesystem>
#include <string>

class Directory
{	Directory(const Directory&) = delete;
	void operator=(const Directory&) = delete;
public:
	std::filesystem::path workdir;
	std::filesystem::path homedir;
	~Directory()
	{}
	Directory(const char* path,const char* datadir)
	{	SetWorkdir();
		SetHomedir(path,datadir);
	}
	bool operator!() const
	{	return homedir.string().size();
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
	std::filesystem::path GetDataFile(const char* subdir,const char* filename) const;
	void SetWorkdir();
	bool SetHomedir(const char* path,const char* datadir);
};

inline
std::ostream& operator<<(std::ostream& os,const Directory& directory)
{	return directory.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Directory& directory)
{	return directory.Input(is);
}

#endif
