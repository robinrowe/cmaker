#FindJPEG.cmake
set(NAME jpeg)
message("*** Found ${NAME}")
option(VERBOSE_PATH ON)

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(JPEG_ROOT ${CMAKELIB_ROOT}/${NAME})

set(JPEG_FOUND ON)
set(JPEG_LIB_VERSION 0)
set(JPEG_INC_PATH ${JPEG_ROOT}/JPEG-build ${JPEG_ROOT}/JPEG-src ${JPEG_ROOT}/JPEG-src/win)
set(JPEG_INCLUDE_DIR ${JPEG_INC_PATH})
set(JPEG_LIBRARY ${NAME}-static)
set(JPEG_LIB_PATH ${CMAKELIB_ROOT}/Release)
#	optimized 	${CMAKELIB_ROOT}/Release
#	debug 		${CMAKELIB_ROOT}/Debug
#	)
if(CMAKELIB)
add_library(JPEG::JPEG ALIAS jpeg-static)
# Must have NASM installed and on path for libjpeg find_program to work:
# https://www.nasm.us/pub/nasm/releasebuilds/2.15.05/win64/
export(TARGETS turbojpeg-static FILE Findturbojpeg-static.cmake)
endif(CMAKELIB)

if(VERBOSE_PATH)
	message("JPEG_ROOT = ${JPEG_ROOT}")
	message("JPEG_INC_PATH = ${JPEG_INC_PATH}")
	message("JPEG_LIBRARY = ${JPEG_LIBRARY}")
	message("JPEG_LIB_PATH = ${JPEG_LIB_PATH}")
endif(VERBOSE_PATH)

include_directories(${JPEG_INC_PATH})
link_directories(${JPEG_LIB_PATH})
link_libraries(${JPEG_LIB})