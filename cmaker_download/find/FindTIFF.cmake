#FindTIFF.cmake
set(NAME tiff)
message("*** Found ${NAME}")
option(VERBOSE_PATH OFF)

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(TIFF_ROOT ${CMAKELIB_ROOT}/${NAME})

set(TIFF_FOUND ON)
set(TIFF_LIB_VERSION 0)
set(TIFF_INC_PATH ${TIFF_ROOT}/${NAME}-build/${NAME} ${TIFF_ROOT}/${NAME}-src/${NAME})
set(TIFF_INCLUDE_DIR ${TIFF_INC_PATH})

find_library(ZLIB NAMES zlibstatic.a zlibstatic)
find_library(JPEG NAMES turbojpeg-static.a turbojpeg-static)

set(TIFF_LIBRARY ${NAME}static)
set(TIFF_LIB_PATH
	optimized 	${JPEG_ROOT}/${NAME}-build/Release/
	debug 		${JPEG_ROOT}/${NAME}-build/Debug/
	)

if(VERBOSE_PATH)
	message("TIFF_LIBRARY = ${TIFF_LIBRARY}")
	message("TIFF_LIB_PATH = ${TIFF_LIB_PATH}")
	message("TIFF_INC_PATH = ${TIFF_INC_PATH}")
endif(VERBOSE_PATH)

include_directories(${TIFF_INC_PATH})
link_directories(${TIFF_LIB_PATH})

