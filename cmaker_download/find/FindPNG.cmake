#FindPNG.cmake
set(NAME png)
message("*** Found ${NAME}")
option(VERBOSE_PATH ON)

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(PNG_ROOT ${CMAKELIB_ROOT}/${NAME})
set(PNG_FOUND ON)
set(PNG_LIB_VERSION 0)
#/c/code/gitlab/cmakelib/build/Win32/Release/libpng16.lib
#/c/code/gitlab/cmakelib/build/Win32/Release/libpng16_static.lib
set(PNG_INC_PATH "${PNG_ROOT}/${NAME}-build" "${PNG_ROOT}/${NAME}-src")
#set(INC_DIR ${CMAKELIB_ROOT}/png/png-src ${CMAKELIB_ROOT}/png/png-build)
set(PNG_INCLUDE_DIR ${PNG_INC_PATH})
set(PNG_LIBRARY ${NAME}static)
set(PNG_LIB_PATH ${CMAKELIB_ROOT}/Release/)
#	optimized 	${PNG_ROOT}/${NAME}-build/Release/
#	debug 		${PNG_ROOT}/${NAME}-build/Debug/
#	find_package(ZLIB REQUIRED)
#find_library(ZLIB NAMES zlibstatic.a zlibstatic)
if(CMAKELIB)
	add_library(PNG::PNG ALIAS libpng)
endif(CMAKELIB)

#This is too late:
#set(SKIP_INSTALL_EXPORT ON CACHE BOOL "Prevent zlibstatic export" FORCE)

#find_package(zlibstatic)
#if(VERBOSE_PATH)
	message("CMAKELIB_ROOT = ${CMAKELIB_ROOT}")
	message("PNG_LIBRARY = ${PNG_LIBRARY}")
	message("PNG_LIB_PATH = ${PNG_LIB_PATH}")
	message("PNG_INC_PATH = ${PNG_INC_PATH}")
# PNG_LIBRARY = pngstatic
# PNG_LIB_PATH = d:/code/gitlab/cmakelib/build/Win32/Release/
# PNG_INC_PATH = d:/code/gitlab/cmakelib/build/Win32/png/png-build;d:/code/gitlab/cmakelib/build/Win32/png/png-src
#endif(VERBOSE_PATH)
#include_directories(${INC_DIR})

find_package(ZLIB REQUIRED)
include_directories(${PNG_INC_PATH})
link_directories(${PNG_LIB_PATH})
link_libraries(${PNG_LIB})


