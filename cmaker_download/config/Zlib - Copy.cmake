#ZLIB.cmake

project("ZLIB")
message("=== Confiuring ${PROJECT_NAME} ===")

set(URL "http://zlib.net/zlib-1.2.11.tar.gz")
set(LIB_PATH "${CMAKE_BINARY_DIR}/${PROJECT_NAME}")
message("LIB_PATH = ${LIB_PATH}")

if(NOT IS_DIRECTORY ${LIB_PATH})
	download_project(PROJ ${PROJECT_NAME}
	PREFIX ${LIB_PATH}
	URL ${URL}
	UPDATE_DISCONNECTED ON
)
#FetchContent_Declare(
#  ${PROJECT_NAME}
#  GIT_REPOSITORY https://github.com/google/googletest.git
#  GIT_TAG        release-1.8.0
#FetchContent_MakeAvailable(googletest)
endif()


#message("${PROJECT_NAME} = ${ZLIB_LIBRARY}")
#set(CMAKE_PREFIX_PATH ${ZLIB_ROOT})
find_package(ZLIB REQUIRED
#    NAMES zlib zlibstatic ZLIBstatic
#    PATHS "${LIB_PATH} NO_DEFAULT_PATH"
)


include_directories(${ZLIB_INCLUDE_DIRS})
add_subdirectory("${LIB_PATH}/${PROJECT_NAME}-src" "${LIB_PATH}/${PROJECT_NAME}-build")
link_directories(${LIB_PATH}/${PROJECT_NAME}-build/Release/)
#target_link_libraries(${PROJECT_NAME}static.lib) 
