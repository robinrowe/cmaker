#NASM.cmake
set(NAME "NASM")
project(${NAME})
message("*** Configuring ${NAME}")

set(NASM_ROOT "${CMAKE_BINARY_DIR}/${NAME}")
set(GIT_REPO http://repo.or.cz/nasm.git)
set(GIT_BRANCH master)

if(NOT IS_DIRECTORY ${NASM_ROOT})
	download_project(PROJ ${NAME}
	PREFIX ${NASM_ROOT}
	GIT_REPOSITORY      ${GIT_REPO}
	GIT_TAG             ${GIT_BRANCH}
	UPDATE_DISCONNECTED ON
)
endif()

add_subdirectory("${NASM_ROOT}/${NAME}-src" "${NASM_ROOT}/${NAME}-build")

