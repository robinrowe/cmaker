#FindOPENEXR.cmake 
set(NAME "libopenexr")
project(${NAME})
message("*** Configuring ${NAME}")

set(OPENEXR_ROOT "${CMAKE_BINARY_DIR}/${NAME}")
#set(GIT_REPO https://github.com/syoyo/tinyexr.git
set(GIT_REPO https://github.com/AcademySoftwareFoundation/openexr.git)
set(GIT_TAG master)

if(NOT IS_DIRECTORY ${OPENEXR_ROOT})
	download_project(PROJ ${NAME}
	PREFIX ${OPENEXR_ROOT}
	GIT_REPOSITORY	${GIT_REPO}     
	GIT_TAG			${GIT_TAG}
	UPDATE_DISCONNECTED ON
)
endif()

if(NOT EXISTS ${OPENEXR_ROOT}/${NAME}-patch)
	include(${CMAKE_CURRENT_LIST_DIR}/modules/set-root-path.cmake)
	set(CONFIG_FILE "OpenEXRConfig.h")
	set(CONFIG_PATH ${ROOT_PATH}/libopenexr/libopenexr-src/cmake)
	set(CONFIG_PATCHED ${CONFIG_PATH}/${CONFIG_FILE})
	message("CONFIG_PATCHED ${CONFIG_PATCHED}")
	file(COPY ${CONFIG_PATH}/${CONFIG_FILE}.in DESTINATION ${CONFIG_PATCHED})
	file(RENAME <oldname> <newname>)
	set(PATCH_1 "s/\\@OPENEXR_NAMESPACE_CUSTOM\\@/ok/")
#@OPENEXR_NAMESPACE_CUSTOM@
#@OPENEXR_INTERNAL_IMF_NAMESPACE@
#@OPENEXR_NAMESPACE_CUSTOM@
#@OPENEXR_IMF_NAMESPACE@
#@OPENEXR_VERSION@"
#@OPENEXR_PACKAGE_NAME@"
#@OPENEXR_VERSION_MAJOR@
#@OPENEXR_VERSION_MINOR@
#@OPENEXR_VERSION_PATCH@
#@OPENEXR_VERSION_EXTRA@"
#@OPENEXR_LIB_VERSION@"
#	message("--- Patching ${NAME} ${FILE_CMakeLists} ---")
#	execute_process(
#		COMMAND sed ${PATCH_1} ${CONFIG_PATCHED} -i
#		WORKING_DIRECTORY ${OPENEXR_ROOT})
endif()