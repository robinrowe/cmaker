#CinePaint FLTK.cmake
set(NAME "fltk")
project(${NAME})
message("=== Building ${NAME} ===")

set(URL http://fltk.org/pub/fltk/snapshots/fltk-1.4.x-r12990.tar.gz)
set(GIT_FLTK "https://github.com/fltk/test-only.git")
set(GIT_TAG "branch-1.4")

set(LIB_PATH "${CMAKE_BINARY_DIR}/fltk" CACHE STRING "${CMAKE_BINARY_DIR}/fltk" )

if(WIN32)
add_definitions(-DWIN32)
endif()

set(OPTION_BUILD_EXAMPLES FALSE CACHE BOOL FALSE)
#set(CMAKE_INCLUDE_PATH /Code/github/vcpkg/installed/x86-windows/include)
set(OPTION_USE_SYSTEM_LIBPNG ON CACHE BOOL ON)
set(OPTION_USE_SYSTEM_LIBJPEG ON CACHE BOOL ON)
set(HAVE_PNG_H TRUE CACHE BOOL TRUE)
# (cmake) -D"OPTION_BUILD_EXAMPLES:BOOL=OFF"
# ./libpng/libpng-build/pnglibconf.h
# ./libjpeg86/libjpeg-build/jconfig.h

include_directories(
	${PNG_INCLUDE_DIR}
	${JPEG_INCLUDE_DIR}
	)

#./libjpeg86/libjpeg-build/Debug/jpeg-static.lib
#./libjpeg86/libjpeg-build/sharedlib/Debug/jpeg.lib
#./zlib/zlib-build/Debug/zlibstaticd.lib

#link_directories(
#	${PATH_LIBJPEG}/libjpeg-build/Debug
#	${PATH_LIBPNG}/libpng-build/Debug 
#	${PATH_ZLIB}/zlib-build/Debug 
#	)

if(NOT IS_DIRECTORY ${LIB_PATH})
message("--- Downloading ${NAME} ---")
download_project(PROJ ${NAME}
	PREFIX              ${LIB_PATH}
#	GIT_REPOSITORY      ${GIT_FLTK}
#	GIT_TAG             ${GIT_TAG}
	URL					${URL}
	UPDATE_DISCONNECTED ON
)
endif()

if(NOT EXISTS ${LIB_PATH}/${NAME}-patch)
	message("--- Patching ${NAME} ---")
	set(PATCH_REGEX "s/add_custom_target\\(uninstall/add_custom_target\\(uninstall-${NAME}/")
	set(PATCH_FILE "${NAME}-src/CMake/install.cmake")
	execute_process(
		COMMAND sed ${PATCH_REGEX} ${PATCH_FILE} -i
		COMMAND touch ${NAME}-patch
		WORKING_DIRECTORY ${LIB_PATH})
endif()

add_subdirectory("${LIB_PATH}/${NAME}-src" "${LIB_PATH}/${NAME}-build")

set(FLTK_LIB_OPT "${LIB_PATH}/${NAME}-build/lib/Release/${NAME}d.lib")
set(FLTK_LIB_DEBUG "${LIB_PATH}/${NAME}-build/lib/Debug/${NAME}d.lib")	
set(FLTK_LIBRARY ${FLTK_LIB_DEBUG} CACHE STRING ${FLTK_LIB_DEBUG})
set(FLTK_IMAGE_LIB_OPT "${LIB_PATH}/${NAME}-build/lib/Release/${NAME}_imagesd.lib")
set(FLTK_IMAGE_LIB_DEBUG "${LIB_PATH}/${NAME}-build/lib/Debug/${NAME}_imagesd.lib")
set(FLTK_IMAGE_LIBRARY ${FLTK_IMAGE_LIB_DEBUG} CACHE STRING ${FLTK_IMAGE_LIB_DEBUG})
#message("FLTK_IMAGE = ${FLTK_IMAGE_LIBRARY}")
	

