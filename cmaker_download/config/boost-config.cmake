set(NAME libboost)

set(BOOST_FOUND ON)
set(BOOST_LIB_VERSION 0)

if(NOT BOOST_ROOT)
set(BOOST_ROOT "/code/gitlab/cmakelib/build/Win32/${NAME}")
endif(NOT BOOST_ROOT)

set(BOOST_LIB ${NAME}.lib)
set(BOOST_LIB_DEBUG "${BOOST_LIB}")
set(BOOST_LIBRARIES ${BOOST_LIB})
set(BOOST_LIBRARY ${BOOST_LIB})
set(BOOST_INC_PATH ${BOOST_ROOT}/${NAME}-build ${BOOST_ROOT}/${NAME}-src)
set(BOOST_INCLUDE_DIR ${BOOST_INC_PATH})
set(BOOST_LIB_PATH ${BOOST_ROOT}/${NAME}-build/Debug)

option(VERBOSE_PATH OFF)
if(VERBOSE_PATH)
	message("BOOST_LIB = ${BOOST_LIB}")
	message("BOOST_LIB_PATH = ${BOOST_LIB_PATH}")
	message("BOOST_INC_PATH = ${BOOST_INC_PATH}")
endif(VERBOSE_PATH)

include_directories(${BOOST_INC_PATH})
link_directories(${BOOST_LIB_PATH})
link_libraries(${BOOST_LIB})

