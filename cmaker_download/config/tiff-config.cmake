set(NAME libtiff)

set(CMAKE_POLICY_DEFAULT_CMP0077 NEW) # This effects it globally

set(TIFF_FOUND ON)
set(TIFF_LIB_VERSION 0)

if(NOT TIFF_ROOT)
set(TIFF_ROOT "/code/gitlab/cmakelib/build/Win32/${NAME}")
endif(NOT TIFF_ROOT)

set(TIFF_LIB "tiffd")
set(TIFF_LIB_DEBUG "${TIFF_LIB}")
set(TIFF_LIBRARIES ${TIFF_LIB})
set(TIFF_LIBRARY ${TIFF_LIB})
set(TIFF_INC_PATH ${TIFF_ROOT}/${NAME}-build/${NAME} ${TIFF_ROOT}/${NAME}-src/${NAME})
set(TIFF_INCLUDE_DIR ${TIFF_INC_PATH})
set(TIFF_LIB_PATH ${TIFF_ROOT}/${NAME}-build/${NAME}/Debug)

option(SHOW_PATH OFF)
if(SHOW_PATH)
	message("TIFF_LIB = ${TIFF_LIB}")
	message("TIFF_LIB_PATH = ${TIFF_LIB_PATH}")
	message("TIFF_INC_PATH = ${TIFF_INC_PATH}")
endif(SHOW_PATH)

include_directories(${TIFF_INC_PATH})
link_directories(${TIFF_LIB_PATH})
link_libraries(${TIFF_LIB})
