#FindImath.cmake 
set(NAME "libimath")
project(${NAME})
message("*** Configuring ${NAME}")

set(IMATH_ROOT "${CMAKE_BINARY_DIR}/${NAME}")
set(GIT_REPO https://github.com/AcademySoftwareFoundation/Imath.git)
set(GIT_TAG master)

if(NOT IS_DIRECTORY ${IMATH_ROOT})
	download_project(PROJ ${NAME}
	PREFIX ${IMATH_ROOT}
	GIT_REPOSITORY	${GIT_REPO}     
	GIT_TAG			${GIT_TAG}
	UPDATE_DISCONNECTED ON
)
endif()

