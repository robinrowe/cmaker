# jpeg-config.cmake
# Copyright 2020 Robin.Rowe@CinePaint.org
# License MIT Open Source

option(VERBOSE_PATH OFF)
set(NAME libjpeg-turbo)
set(JPEG_FOUND ON)
set(JPEG_LIB_VERSION 0)

if(NOT JPEG_ROOT)
	set(JPEG_ROOT "/code/gitlab/cmakelib/build/Win32/${NAME}")
	if(VERBOSE_PATH)
		message("JPEG_ROOT = ${JPEG_ROOT}")
	endif(VERBOSE_PATH)
endif(NOT JPEG_ROOT)

#./libjpeg-turbo/libjpeg-turbo-build/Debug/jpeg-static.lib

set(JPEG_LIB jpeg-static)
set(JPEG_LIB_DEBUG ${JPEG_LIB})
set(JPEG_LIBRARIES ${JPEG_LIB})
set(JPEG_LIBRARY ${JPEG_LIB})
set(JPEG_INC_PATH ${JPEG_ROOT}/${NAME}-build ${JPEG_ROOT}/${NAME}-src)
set(JPEG_INCLUDE_DIR ${JPEG_INC_PATH})
set(JPEG_LIB_PATH ${JPEG_ROOT}/${NAME}-build/Debug)

if(VERBOSE_PATH)
	message("JPEG_LIB = ${JPEG_LIB}")
	message("JPEG_LIB_PATH = ${JPEG_LIB_PATH}")
	message("JPEG_INC_PATH = ${JPEG_INC_PATH}")
endif(VERBOSE_PATH)

include_directories(${JPEG_INC_PATH})
link_directories(${JPEG_LIB_PATH})
link_libraries(${JPEG_LIB})