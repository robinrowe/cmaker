#CURSES.cmake
set(NAME "CURSES")
project(${NAME})
message("=== Building ${NAME} ===")

set(URL "ftp://ftp.invisible-island.net/ncurses/ncurses.tar.gz")
set(LIB_PATH "${CMAKE_BINARY_DIR}/${NAME}")
#message("LIB_PATH = ${LIB_PATH}")

set(CURSES_ROOT ${LIB_PATH} 
	CACHE STRING ${LIB_PATH})
set(CURSES_INCLUDE_DIRS "${LIB_PATH}/${NAME}-build" "${LIB_PATH}/${NAME}-src")
set(CURSES_INCLUDE_DIR ${CURSES_INCLUDE_DIRS} PARENT_SCOPE)
#	CACHE STRING ${CURSES_INCLUDE_DIRS})
#message("CURSES_INCLUDE_DIR = ${CURSES_INCLUDE_DIR}")
set(CURSES_FOUND TRUE PARENT_SCOPE)
#	CACHE BOOL TRUE)

#${LIB_PATH}/${NAME}-build/Release/
set(CURSES_LIB ${NAME}static.lib)
set(CURSES_LIB_DEBUG "${LIB_PATH}/${NAME}-build/Debug/${NAME}staticd.lib")
set(CURSES_LIBRARY ${CURSES_LIB} PARENT_SCOPE)
#	CACHE STRING ${CURSES_LIB})
set(CURSES_LIBRARIES ${CURSES_LIB} PARENT_SCOPE)
#	CACHE STRING ${CURSES_LIB})

if(NOT IS_DIRECTORY ${LIB_PATH})
	download_project(PROJ ${NAME}
	PREFIX ${LIB_PATH}
	URL ${URL}
	UPDATE_DISCONNECTED ON
)
endif()

include_directories(${CURSES_INCLUDE_DIRS})
add_subdirectory("${LIB_PATH}/${NAME}-src" "${LIB_PATH}/${NAME}-build")
link_directories(${LIB_PATH}/${NAME}-build/Release/)
#target_link_libraries(${NAME}static.lib) 

#message("${NAME} = ${CURSES_LIBRARY}")
#set(CMAKE_PREFIX_PATH ${CURSES_ROOT})
find_package(CURSES REQUIRED
    NAMES CURSES CURSESstatic CURSESstatic
    PATHS "${LIB_PATH} NO_DEFAULT_PATH"
)
