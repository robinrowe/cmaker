#LibBoost.cmake
set(NAME "libboost")
project(${NAME})
message("-- Configuring ${NAME}")

set(URL https://dl.bintray.com/boostorg/release/1.76.0/source/boost_1_76_0.tar.bz2)
set(BOOST_ROOT "${CMAKE_BINARY_DIR}/${NAME}")

if(NOT IS_DIRECTORY ${BOOST_ROOT})
download_project(PROJ ${NAME}
	PREFIX			${BOOST_ROOT}
	URL ${URL}
	UPDATE_DISCONNECTED ON
)
endif()

if(NOT EXISTS ${BOOST_ROOT}/${NAME}-patch)
	message("Do something with boost...")
	set(FILE_CMakeLists "${NAME}-src/CMakeLists.txt")
#	set(PATCH_CMP0022 "s/cmake_policy\\(SET\\ CMP0022/#cmake_policy(SET\\ CMP0022/")
#	execute_process(
#		COMMAND touch ${NAME}-patch
#		WORKING_DIRECTORY ${BOOST_ROOT})		
endif()

#set(WITH_CRT_DLL TRUE)
#set_property(TARGET jpeg-static PROPERTY
#             MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
#add_subdirectory("${BOOST_ROOT}/${NAME}-src" "${BOOST_ROOT}/${NAME}-build")
