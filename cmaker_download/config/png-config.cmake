set(NAME libpng)

set(PNG_FOUND ON)
set(PNG_LIB_VERSION 0)

if(NOT PNG_ROOT)
set(PNG_ROOT "/code/gitlab/cmakelib/build/Win32/${NAME}")
endif(NOT PNG_ROOT)

set(PNG_LIB "${NAME}16_static.lib")
set(PNG_LIB_DEBUG ${PNG_LIB})
set(PNG_LIBRARIES ${PNG_LIB})
set(PNG_LIBRARY ${PNG_LIB})
set(PNG_INC_PATH "${LIB_PATH}/${NAME}-build" "${LIB_PATH}/${NAME}-src")
set(PNG_INCLUDE_DIR ${PNG_INC_PATH})
set(PNG_LIB_PATH ${PNG_ROOT}/${NAME}-build/Debug)

option(VERBOSE_PATH OFF)
if(VERBOSE_PATH)
	message("PNG_LIB = ${PNG_LIB}")
	message("PNG_LIB_PATH = ${PNG_LIB_PATH}")
	message("PNG_INC_PATH = ${PNG_INC_PATH}")
endif(VERBOSE_PATH)

include_directories(${PNG_INC_PATH})
link_directories(${PNG_LIB_PATH})
link_libraries(${PNG_LIB})


