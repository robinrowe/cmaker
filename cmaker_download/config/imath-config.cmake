set(NAME libimath)

set(IMATH_FOUND ON)
set(IMATH_LIB_VERSION 0)

if(NOT IMATH_ROOT)
set(IMATH_ROOT "/code/gitlab/cmakelib/build/Win32/${NAME}")
endif(NOT IMATH_ROOT)

set(IMATH_LIB ${NAME}.lib)
set(IMATH_LIB_DEBUG "${IMATH_LIB}")
set(IMATH_LIBRARIES ${IMATH_LIB})
set(IMATH_LIBRARY ${IMATH_LIB})
set(IMATH_INC_PATH ${IMATH_ROOT}/${NAME}-build ${IMATH_ROOT}/${NAME}-src/src)
set(IMATH_INCLUDE_DIR ${IMATH_INC_PATH})
set(IMATH_LIB_PATH ${IMATH_ROOT}/${NAME}-build/Debug)

option(VERBOSE_PATH OFF)
if(VERBOSE_PATH)
	message("IMATH_INC_PATH = ${IMATH_INC_PATH}")
	message("IMATH_LIB = ${IMATH_LIB}")
	message("IMATH_LIB_PATH = ${IMATH_LIB_PATH}")
endif(VERBOSE_PATH)

include_directories(${IMATH_INC_PATH})
link_directories(${IMATH_LIB_PATH})
link_libraries(${IMATH_LIB})

