#READLINE.cmake
set(NAME "READLINE")
project(${NAME})
message("=== Building ${NAME} ===")

set(URL "http://deb.debian.org/debian/pool/main/r/readline/readline_7.0.orig.tar.gz")
set(LIB_PATH "${CMAKE_BINARY_DIR}/${NAME}")
#message("LIB_PATH = ${LIB_PATH}")

set(READLINE_ROOT ${LIB_PATH} 
	CACHE STRING ${LIB_PATH})
set(READLINE_INCLUDE_DIRS "${LIB_PATH}/${NAME}-build" "${LIB_PATH}/${NAME}-src")
set(READLINE_INCLUDE_DIR ${READLINE_INCLUDE_DIRS} PARENT_SCOPE)
#	CACHE STRING ${READLINE_INCLUDE_DIRS})
#message("READLINE_INCLUDE_DIR = ${READLINE_INCLUDE_DIR}")
set(READLINE_FOUND TRUE PARENT_SCOPE)
#	CACHE BOOL TRUE)

#${LIB_PATH}/${NAME}-build/Release/
set(READLINE_LIB ${NAME}static.lib)
set(READLINE_LIB_DEBUG "${LIB_PATH}/${NAME}-build/Debug/${NAME}staticd.lib")
set(READLINE_LIBRARY ${READLINE_LIB} PARENT_SCOPE)
#	CACHE STRING ${READLINE_LIB})
set(READLINE_LIBRARIES ${READLINE_LIB} PARENT_SCOPE)
#	CACHE STRING ${READLINE_LIB})

if(NOT IS_DIRECTORY ${LIB_PATH})
	download_project(PROJ ${NAME}
	PREFIX ${LIB_PATH}
	URL ${URL}
	UPDATE_DISCONNECTED ON
)
endif()

include_directories(${READLINE_INCLUDE_DIRS})
add_subdirectory("${LIB_PATH}/${NAME}-src" "${LIB_PATH}/${NAME}-build")
link_directories(${LIB_PATH}/${NAME}-build/Release/)
#target_link_libraries(${NAME}static.lib) 

#message("${NAME} = ${READLINE_LIBRARY}")
#set(CMAKE_PREFIX_PATH ${READLINE_ROOT})
find_package(READLINE REQUIRED
    NAMES READLINE READLINEstatic READLINEstatic
    PATHS "${LIB_PATH} NO_DEFAULT_PATH"
)
