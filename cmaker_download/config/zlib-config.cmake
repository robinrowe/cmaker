project("zlib")
message("--- ${PROJECT_NAME}-config.cmake")
option(VERBOSE_PATH OFF)

set(ZLIB_FOUND ON)
set(ZLIB_LIB_VERSION 0)
set(ZLIB_INC_PATH ${ZLIB_ROOT}/ZLIB-build ${ZLIB_ROOT}/ZLIB-src)
set(ZLIB_INCLUDE_DIR ${ZLIB_INC_PATH})
#set(ZLIB_LIB_PATH ${ZLIB_ROOT}/${PROJECT_NAME}-build/Debug)
#set(ZLIB_LIB_PATH
#	optimized 	${PATH_LIBUNISTD}/${PROJECT_NAME}-build/Release/
#	debug 		${PATH_LIBUNISTD}/${PROJECT_NAME}-build/Debug/
#
#)	
#set(ZLIB_LIBRARIES ${ZLIB_LIB})
#
#
set(ZLIB_LIBRARY ${PROJECT_NAME}-static.lib}
	CACHE STRING ${ZLIB_LIB}
	)

if(VERBOSE_PATH)
#	message("ZLIB_ROOT = ${ZLIB_ROOT}")
	message("ZLIB_LIBRARY = ${ZLIB_LIBRARY}")
	message("ZLIB_LIB_PATH = ${ZLIB_LIB_PATH}")
	message("ZLIB_INC_PATH = ${ZLIB_INC_PATH}")
endif(VERBOSE_PATH)

include_directories(${ZLIB_INC_PATH})
link_directories(${ZLIB_LIB_PATH})
