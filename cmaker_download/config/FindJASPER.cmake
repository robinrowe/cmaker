#FindJASPER.cmake 
set(NAME "libjasper")
project(${NAME})
message("*** Configuring ${NAME}")

set(JASPER_ROOT "${CMAKE_BINARY_DIR}/${NAME}")
set(GIT_REPO https://github.com/jasper-software/jasper.git)
set(GIT_TAG master)

if(NOT IS_DIRECTORY ${JASPER_ROOT})
	download_project(PROJ ${NAME}
	PREFIX ${JASPER_ROOT}
	GIT_REPOSITORY	${GIT_REPO}     
	GIT_TAG			${GIT_TAG}
	UPDATE_DISCONNECTED ON
)
endif()

include(${CMAKE_CURRENT_SOURCE_DIR}/../cmake/modules/jasper-config.cmake)

include_directories(${JASPER_INCLUDE_DIRS})
add_subdirectory("${JASPER_ROOT}/${NAME}-src" "${JASPER_ROOT}/${NAME}-build")
link_directories(${JASPER_ROOT}/${CMAKE_PROJECT_NAME}-build/Release/)

