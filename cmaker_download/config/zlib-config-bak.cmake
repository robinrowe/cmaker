#ZLIB-config.cmake

# set(ZLIB_ROOT "${CMAKE_BINARY_DIR}/ZLIB")
set(NAME zlib)
option(VERBOSE_PATH OFF)

set(ZLIB_FOUND ON)
set(ZLIB_LIB_VERSION 0)
#if(NOT ZLIB_ROOT)
#	set(ZLIB_ROOT "${ROOT_PATH}/${NAME}")
#endif(NOT ZLIB_ROOT)

set(ZLIB_LIB ${NAME}static)
set(ZLIB_LIB_DEBUG ${ZLIB_LIB})
set(ZLIB_LIBRARIES ${ZLIB_LIB})
set(ZLIB_LIBRARY ${ZLIB_LIB})
set(ZLIB_INC_PATH ${ZLIB_ROOT}/ZLIB-build ${ZLIB_ROOT}/ZLIB-src)
set(ZLIB_INCLUDE_DIR ${ZLIB_INC_PATH})
set(ZLIB_LIB_PATH ${ZLIB_ROOT}/${NAME}-build/Debug)

if(VERBOSE_PATH)
	message("ZLIB_ROOT = ${ZLIB_ROOT}")
	message("ZLIB_LIB = ${ZLIB_LIB}")
	message("ZLIB_LIB_PATH = ${ZLIB_LIB_PATH}")
	message("ZLIB_INC_PATH = ${ZLIB_INC_PATH}")
endif(VERBOSE_PATH)

include_directories(${ZLIB_INC_PATH})
link_directories(${ZLIB_LIB_PATH})
#link_libraries(${ZLIB_LIB})

