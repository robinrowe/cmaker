# cmakelib CHANGES.md

Robin Rowe 2020/12/17

- PNG builds
- JPEG builds
- ZLIB builds
- TIFF builds
- OPENEXR works?
- JASPER broken

Dec 17: TiFF builds
Dec 17: Added OpenEXR
Dec 17: Added JASPER, has cmake include issues
Dec 16: Enabled TIFF, has build flag issues
