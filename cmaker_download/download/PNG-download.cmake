#PNG-download.cmake

set(NAME png)
message("*** Download ${NAME}")

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(PNG_ROOT ${CMAKELIB_ROOT}/${NAME})
set(PNG_TESTS OFF)

#set(FTP ftp://ftp-osl.osuosl.org/pub/libpng/src)
#set(URL ${FTP}/libpng16/libpng-1.6.34.tar.gz)
set(URL https://downloads.sourceforge.net/project/libpng/libpng16/1.6.37/libpng-1.6.37.tar.gz)

if(IS_DIRECTORY ${PNG_ROOT})
	message("Found downloaded ${NAME} ${PNG_ROOT}")
else(IS_DIRECTORY ${PNG_ROOT})
	download_project(PROJ ${NAME}
	PREFIX ${PNG_ROOT}
	URL ${URL}
	UPDATE_DISCONNECTED ON
	)
endif(IS_DIRECTORY ${PNG_ROOT})

set(SRC_PATH "${PNG_ROOT}/${NAME}-src")
file(COPY "${SRC_PATH}/scripts/pnglibconf.h.prebuilt" DESTINATION "${SRC_PATH}")
file(RENAME "${SRC_PATH}/pnglibconf.h.prebuilt" "${SRC_PATH}/pnglibconf.h")