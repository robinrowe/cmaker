#zlib-download.cmake

set(NAME zlib)
message("*** Download ${NAME}")

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(ZLIB_ROOT ${CMAKELIB_ROOT}/${NAME})

#set(URL "http://zlib.net/zlib-1.2.11.tar.gz")
#set(URL http://zlib.net/zlib-1.2.12.tar.gz)
set(URL http://zlib.net/zlib-1.2.13.tar.gz)

if(IS_DIRECTORY ${ZLIB_ROOT})
	message("Found downloaded ${NAME} ${ZLIB_ROOT}")
else(IS_DIRECTORY ${ZLIB_ROOT})
	download_project(PROJ ${NAME}
		PREFIX ${ZLIB_ROOT}
		URL ${URL}
		UPDATE_DISCONNECTED ON
	)
endif(IS_DIRECTORY ${ZLIB_ROOT})
message("LIBPATH = ${LIBPATH}")

