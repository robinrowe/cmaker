#TIFF-download.cmake

set(NAME tiff)
message("*** Download ${NAME}")

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(TIFF_ROOT ${CMAKELIB_ROOT}/${NAME})

set(GIT_REPO https://gitlab.com/libtiff/libtiff.git)
set(GIT_TAG master)
set(BUILD_SHARED_LIBS FALSE)

if(IS_DIRECTORY ${TIFF_ROOT})
	message("Found downloaded ${NAME} ${TIFF_ROOT}")
else(IS_DIRECTORY ${TIFF_ROOT})
	download_project(PROJ ${NAME}
	PREFIX ${TIFF_ROOT}
	GIT_REPOSITORY	${GIT_REPO}     
	GIT_TAG			${GIT_TAG}
	UPDATE_DISCONNECTED ON
	)
endif(IS_DIRECTORY ${TIFF_ROOT})

#include(${CMAKE_CURRENT_SOURCE_DIR}/../cmake/modules/tiff-config.cmake)

#  Support for external codecs:
#   ZLIB support:                       ON (requested) TRUE (availability)
#   libdeflate support:                 ON (requested) FALSE (availability)
#   Pixar log-format algorithm:         ON (requested) TRUE (availability)
#   JPEG support:                       ON (requested) TRUE (availability)
#   Old JPEG support:                   ON (requested) TRUE (availability)
#   JPEG 8/12 bit dual mode:            ON (requested) FALSE (availability)
#   ISO JBIG support:                   ON (requested) FALSE (availability)
#   LZMA2 support:                      ON (requested) FALSE (availability)
#   ZSTD support:                       ON (requested)  (availability)
#   WEBP support:                       ON (requested) FALSE (availability)
#   C++ support:                        ON (requested) TRUE (availability)
#   OpenGL support:                     FALSE
