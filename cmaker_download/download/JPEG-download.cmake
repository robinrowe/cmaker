#JPEG-download.cmake

set(NAME jpeg)
message("*** Download ${NAME}")

if(NOT CMAKELIB_ROOT)
	message(FATAL_ERROR "ERROR: no CMAKELIB_ROOT!")
endif(NOT CMAKELIB_ROOT)
set(JPEG_ROOT ${CMAKELIB_ROOT}/${NAME})

#set(URL https://downloads.sourceforge.net/project/libjpeg-turbo/1.5.3/libjpeg-turbo-1.5.3.tar.gz)
set(URL https://downloads.sourceforge.net/project/libjpeg-turbo/2.1.3/libjpeg-turbo-2.1.3.tar.gz)
#set(GIT_REPO https://github.com/mozilla/mozjpeg.git)
set(GIT_REPO https://github.com/libjpeg-turbo/libjpeg-turbo.git)
set(GIT_TAG master)

set(NASM-NOTFOUND)
set(WITH_12BIT FALSE)
set(WITH_SIMD TRUE) 
set(WITH_ARITH_ENC TRUE)
set(WITH_ARITH_DEC TRUE)
set(ENABLE_STATIC TRUE)

if(IS_DIRECTORY ${JPEG_ROOT})
	message("Found downloaded ${NAME} ${JPEG_ROOT}")
else(IS_DIRECTORY ${JPEG_ROOT})
	download_project(PROJ ${NAME}
		PREFIX			${JPEG_ROOT}
		URL				${URL}
		UPDATE_DISCONNECTED ON
	)
endif(IS_DIRECTORY ${JPEG_ROOT})

if(NOT EXISTS ${JPEG_ROOT}/${NAME}-patch)
	set(FILE_CMakeLists "${NAME}-src/CMakeLists.txt")
#	set(PATCH_CMP0022 "s/cmake_policy\\(SET\\ CMP0022/#cmake_policy(SET\\ CMP0022/")
	set(PATCH_CMP0022 "s/\\(cmake_policy(SET CMP0022\\)/#\\1/") 
#	message(${PATCH_CMP0022})
	set(PATCH_CURRENT "s/CMAKE_SOURCE_DIR/CMAKE_CURRENT_SOURCE_DIR/g")
	set(PATCH_CURRENT_UP "s|CMAKE_SOURCE_DIR}|CMAKE_CURRENT_SOURCE_DIR}/..|g")
	set(PATCH_INTERFACE "s/INTERFACE_LINK_LIBRARIES/LINK_INTERFACE_LIBRARIES/g")
	set(PATCH_LINK "s/set_target_properties(turbojpeg PROPERTIES LINK_INTERFACE_LIBRARIES/#set_target_properties(turbojpeg PROPERTIES LINK_INTERFACE_LIBRARIES")
	set(FILE_sharedlib "${NAME}-src/sharedlib/CMakeLists.txt")
	message("--- Patching ${NAME} ${FILE_CMakeLists} ---")
	execute_process(
		COMMAND sed ${PATCH_CMP0022} ${FILE_CMakeLists} -i
		WORKING_DIRECTORY ${JPEG_ROOT})
	execute_process(
		COMMAND sed ${PATCH_CURRENT} ${FILE_CMakeLists} -i
		WORKING_DIRECTORY ${JPEG_ROOT})
	execute_process(
		COMMAND sed ${PATCH_INTERFACE} ${FILE_CMakeLists} -i
		WORKING_DIRECTORY ${JPEG_ROOT})
	execute_process(
		COMMAND sed ${PATCH_LINK} ${FILE_CMakeLists} -i
		WORKING_DIRECTORY ${JPEG_ROOT})
	message("--- Patching ${NAME} ${FILE_sharedlib} ---")
	execute_process(
		COMMAND sed ${PATCH_CURRENT_UP} ${FILE_sharedlib} -i
		WORKING_DIRECTORY ${JPEG_ROOT})
	set(FILE_INC "${NAME}-src/simd/jsimdext.inc")
	message("--- Patching ${NAME} ${FILE_INC} ---")
	set(PATCH_INC "s|jsimdcfg\.inc|\.\./win/jsimdcfg\.inc|g")
	execute_process(
		COMMAND sed ${PATCH_INC} ${FILE_INC} -i
		WORKING_DIRECTORY ${JPEG_ROOT})
	execute_process(
		COMMAND touch ${NAME}-patch
		WORKING_DIRECTORY ${JPEG_ROOT})		
endif()

set(WITH_CRT_DLL TRUE)
