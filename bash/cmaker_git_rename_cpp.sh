#!/bin/sh
# cmaker_rename_cpp.sh
# Copyright 2024/12/7 Robin.Rowe@Cinepaint.org
# License open source MIT

RenameCpp()
{	#printf '%s\n' "$1"
	filename=${1%.*}$2
#	filename=${filename}${to}
	echo git mv $1 ${filename}
	git mv $1 ${filename}
}

Walkdir()
{	path=$1
#	echo check: $1 $2 $3
	for filename in "$path"/*; do
		if [ -d "$filename" ]; then
			Walkdir "$filename" $2 $3
			continue
		fi
		if [[ $filename == *$2 ]]; then
			#echo matched: $filename $2
			RenameCpp $filename $3
		fi
	done
}

Walkdir . .c .cpp
Walkdir . .cxx .cpp
Walkdir . .H .h
Walkdir . .hpp .h

