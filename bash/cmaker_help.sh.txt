Cmaker is a cmake build system generator created and maintained by Robin Rowe.
Works in Linux, MacOS and in Windows gitbash. Implemented in bash.

cmaker_help.sh: 
	cmaker_help

*** C++ Build System ***

cmaker_program.sh: 
	cmaker_program
cmaker_project.sh: 
	cmaker_project
cmaker_class.sh: 
	cmaker_class

*** C Build System ***

cmaker_function.sh: 
	cmaker_function

cmaker_struct.sh: 
	cmaker_struct
	
*** Cmake Fameworks ***

cmaker_cmake.sh: 
	cmaker_cmake
cmaker_examples.sh: 
	cmaker_examples
cmaker_file.sh: 
	cmaker_file

*** Code Utilities ***

cmaker_ctags.sh: 
	cmaker_ctags
cmaker_kloc.sh: 
	cmaker_kloc

*** Contact ***

Robin.Rowe@CinePaint.org
323-535-0952 Washington D.C. timezone

*** Experimental Stuff, May Ignore... ***

cmaker_library.sh: 
	cmaker_library
cmaker_merge.sh: 
	cmaker_merge
cmaker_no_spaces.sh: 
	cmaker_no_spaces
cmaker_rename_cpp.sh: 
	cmaker_rename_cpp
cmaker_rm_vcxproj.sh: 
	cmaker_rm_vcxproj
cmaker_setup.sh: 
	cmaker_setup
cmaker_sources.sh: 
	cmaker_sources
cmaker_to_cpp.sh: 
	cmaker_to_cpp
cmaker_top.sh: 
	cmaker_top
cmaker_all.sh: 
	cmaker_all
cmaker_class_remove.sh: 
	cmaker_class_remove
cmaker_dll.sh: 
	cmaker_dll
