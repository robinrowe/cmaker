#!/bin/bash 
# cmaker_project.sh
# Created by Robin Rowe 2021/2/15
# License MIT open source

if [ -z "$AUTHOR" ]; then 
	export AUTHOR="Robin Rowe"
fi
if [[ ! -e LICENSE ]]; then
	echo "Trade secret of Venture Hollywood" > ./LICENSE
fi
echo $AUTHOR
cat LICENSE