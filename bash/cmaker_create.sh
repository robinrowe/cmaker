#!/bin/bash 
# cmaker_generate.sh
# Created by Robin Rowe 2019/3/6
# License MIT open source

project_name=$1
self=$0
pwd=$PWD
date=$(date +%Y-%m-%d)

ReadLicenseFile()
{	if [[ ! -e LICENSE ]]; then
		echo "Missing LICENSE file"
		exit 1
	fi
	read -r license < LICENSE
	echo "License: ${license}"
}

Sed()
{	local args=$1
	local file=$2
#	echo "sed "${args}" ${file}"
	sed "${args}" ${file} > ${file}.tmp
	mv -f ${file}.tmp ${file}
}

SetProjectName()
{	project_name=$1
	if [ "${project_name}" = "." ]; then 
		return
	fi
	if [ "${project_name:0:1}" = "." ]; then 
		project_name="${project_name:2}" # trim ./
	fi
	project_name=${project_name//\//_}
#	eval "$1='${project_name}'" 
}
   
CreateCmakeList() # project_name path project_file
{	local project_name=$1
	local path=$2
	local project_file=$3
#	echo "CmakeList: $1 $2 $3"
	SetProjectName ${project_name}
#	echo "set project_name: ${project_name}"
	local cmakelist=${path}/CMakeLists.txt
	if [[ -e ${cmakelist} ]]; then
		echo "Skipping... ${cmakelist} already exists"
		return
	fi
#	echo Creating project ${project_name}...
#	echo "	cp ${project_file} ${cmakelist}"
	cp ${project_file} ${cmakelist}
	Sed "s|CMAKER_PROJECT|${project_name}|g" ${cmakelist}
	Sed "s|CMAKER_DATE|${date}|g" ${cmakelist}
	Sed "s|CMAKER_AUTHOR|${AUTHOR}|g" ${cmakelist}
	Sed "s|CMAKER_LICENSE|${license}|g" ${cmakelist}
}

AddSubdir() # path
{	local path=$1
	if [ "${path}" = "." ]; then 
		return
	fi
	local subdir=$(basename ${path})
	local basedir=$(dirname ${self})
	local cmakelist=${pwd}/CMakeLists.txt
	echo "add_subdirectory(${path}) >> ${cmakelist}"
	echo "add_subdirectory(${path})" >> ${cmakelist}
}

AddDirSources() # path
{	local path=$1
	files=(${path}/*.{h,c,cc,cpp,cxx})
	local count=${#files[@]}
#	echo "Analyzing ${path}: ${count} source file(s)"
	if [ ${count} -eq 0 ]; then
#		echo "No sources in $path"
		return
	fi
	local cmakelist=${path}/CMakeLists.txt
	local msg="Adding ${path}: ${count} source file"
	if [ ${count} -gt 1 ]; then
		msg=${msg}"s"
	fi
	echo ${msg}
#	echo ${msg} >> ${cmakelist}
#	echo ${files}
#	dir=${dir:2}
	echo "file(STRINGS sources.cmake SOURCES)" >> ${cmakelist}
	sources=${path}/sources.cmake
	echo "sources.cmake" > ${sources}
	for ((i=0; i < ${count}; i++)); do
		local file="${files[$i]}"
#		echo "file: ${file}"
		local filename=$(basename $file)
#		echo "${filename} -> ${sources}"
		echo ${filename} >> ${sources}
	done
	Sed "s|CMAKER_COUNT|${count}|g" ${cmakelist}
	if [ "$CMAKERLIB" = true ]; then 
		echo "add_library(\${MODULE_NAME} OBJECT ${SOURCES})" >> ${cmakelist}
		echo "link_libraries(${subdir})" >> ${cmakelist}
	else
		echo "add_executable(\${MODULE_NAME} \${SOURCES})" >> ${cmakelist}
	fi
	AddSubdir ${path}
}

AddSubdirs()
{	local path=$1
#	echo "path ${path}"
	if [ "${path}" = "./.git" ] || [ "${dir}" = "./build" ]; then 
		echo "Ignoring ${path}"
		return
	fi
#	echo "Search ${path}"
	if [ "${path}" != "." ]; then 
		CreateCmakeList ${path} ${path} ${self}.dir.txt
#		AddSubdir ${path}
		AddDirSources ${path}
	fi
	for dir in ${path}/*; do
		if [ -d "${dir}" ]
		then
			AddSubdirs "${dir}"
		fi
	done
#	AddDirSources ${path}
#	AddSubdir ${path}
}

Clean()
{	local path=$1
	if [ "${path}" = ".git" ] || [ "${path}" = "./build" ]; then 
		return
	fi
#	echo "Clean ${path}"
#	echo "Clean ${path}/CMakeLists.txt ${path}/sources.cmake"
	rm -f ${path}/CMakeLists.txt ${path}/sources.cmake
	for dir in ${path}/*; do
		if [ -d "${dir}" ]
		then
			Clean "${dir}"
		fi
	done
}

main()
{	if [ -z "$AUTHOR" ]; then 
		echo "In bash set your name: % export AUTHOR=\"Your Name\""
		exit 1
	fi
	if [ -z "${project_name}" ]; then 
		echo "syntax: $0 PROJECT_NAME"
		echo "$0 generates cmake build system"
		exit 1
	fi
	if [ "$CMAKERLIB" = true ]; then 
		echo "CMAKERLIB = true. Creating library, not programs"
	else
		echo "CMAKERLIB != true. Creating programs, not library"
		echo "If want library build, in bash set: % export CMAKERLIB=true"
	fi
	ReadLicenseFile
	echo "  project = ${project_name}"
	echo "  date = ${date}"
	echo "  author = ${AUTHOR}"
	echo "  license = ${license}"
	Clean .
	echo "Removed any CMakeLists.txt or sources.cmake files"
	CreateCmakeList ${project_name} . ${self}.txt
	shopt -s nullglob dotglob
#	shopt -s nullglob # Sets nullglob
# dotglob: includes filenames beginning with a ‘.’ 
# nocaseglob: matches filenames in a case-insensitive fashion
# nullglob: patterns which match no files to expand to a null string, rather than themselves
# failglob: patterns which fail to match filenames during filename expansion result in an expansion error
	AddSubdirs .
	AddDirSources .
	shopt -u nullglob # Unsets nullglob
#	tail ${cmakelist}
#	echo "$OSTYPE done!"
if [ "$OSTYPE" = "msys" ]; then
	echo "You may next: 'mkdir build; cd build; cmake .. -A x64'"
else	
	echo "You may next: 'mkdir build; cd build; cmake ..'"
fi
}

main

