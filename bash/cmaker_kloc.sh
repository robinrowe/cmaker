#!/bin/bash
# cmaker_kloc.sh
# Created by Robin Rowe 2021/9/28
# License MIT open source

#set -e
#set -u
let total_file_count=0
let total_loc=0
verbose=$1

CountLines()
{	local input=$1
#	echo $1
	let loc=0
	while IFS= read -r line
	do
#		echo "$line"
		let loc++
	done < "${input}"
	let total_loc+=loc
	let total_file_count++
	commas=$(printf "%'d" ${loc})
	if [ "${verbose}" == "-v" ]; then
		echo "${total_file_count}. ${file}: ${commas}"
	else
		printf "." 1>&2
	fi
}

CountFiles()
{	local dir=$1
#	echo "Count ${dir}"
	files=(${dir}/*.{h,c,cc,cpp,cxx})
	local file_count=${#files[@]}
	if [ ${file_count} -eq 0 ]; then
#		echo "Skipping $dir"
		return
	fi
#	echo ${files}
	dir=${dir:2}
#	sources=${PWD}/${dir}/sources.cmake
	for ((i=0; i < ${file_count}; i++)); do
		local file="${files[$i]}"
		CountLines ${file}
	done
}

RecursePath() 
{	local path=$1
#	echo "Search ${path}"
	for dir in ${path}/*; do
		if [ -d "${dir}" ] && [ ${dir} != ".git" ]
		then
			RecursePath "${dir}"
		fi
	done
	CountFiles ${path}
}

main()
{
#	echo $0
	if [ "${verbose}" == "-v" ]; then
		echo "verbose mode"
	fi
	shopt -s nullglob dotglob
	RecursePath .
	shopt -u nullglob # Unsets nullglob
	echo "cmaker_kloc: ${PWD##*/}"
	let kloc=(total_loc+500)/1000
	commas=$(printf "%'d" ${total_loc}) 
	echo "C/C++ source file count = ${total_file_count}, kloc = ${kloc}k (${commas})"
}

main