#!/bin/bash 
# cmaker_top.sh
# Created by Robin Rowe 2019/3/6
# License MIT open source

cmakelist=${PWD}/CMakeLists.txt
date=$(date +%Y-%m-%d)
project=$1
project_file="$0.txt"

ReadLicenseFile()
{	if [[ ! -e LICENSE ]]; then
		echo "Missing LICENSE file"
		exit 1
	fi
	read -r license < LICENSE
	echo "License: ${license}"
}

Sed()
{	local arg=$1
	local file=$2
#	echo "sed "${arg}" ${file}"
	sed "${arg}" ${file} > ${file}.tmp
	mv -f ${file}.tmp ${file}
}

CreateCmakeList() 
{	if [[ -e ${cmakelist} ]]; then
		echo "Skipping... ${cmakelist} already exists"
		return
	fi
	local project=$1
	echo Creating ${cmakelist} for project ${project}...
	cp ${project_file} ${cmakelist}
	Sed "s/CMAKER_PROJECT/${project}/g" ${cmakelist}
	Sed "s/DATE/${date}/g" ${cmakelist}
	Sed "s/AUTHOR/${AUTHOR}/g" ${cmakelist}
	Sed "s/LICENSE/${license}/g" ${cmakelist}
}

AddPrograms()
{	local dir=$1
#	echo "AddPrograms ${dir}"
	for file in ${dir}/*.{c,cc,cpp,cxx}; do
		#local source=${file##*/} 
		#local base=${source%.*}
#		local base=$(basename $file)
#		local base=$(basename $(dirname $file))
		local base=$(basename $(dirname $file))
		base=${base}_$(basename $file)
		base=${base%.*}
		base=${base//-/_}
		base=${base// /_}
		base=${base//\./_}
		base=${base//__/_}
#		echo "Split: $file ${base} ${source}"
		echo "add_executable(test_${base} ${file})" 
		echo "add_executable(test_${base} ${file})" >> ${cmakelist}
	done
}


AddSubdirs()
{	local path=$1
#	echo "Search ${path}"
	for dir in ${path}/*; do
#		dir=${dir%?}; # remove /
		if [ -d "${dir}" ]
		then
#			echo "${dir}"
			AddPrograms "${dir}"
			AddSubdirs "${dir}"
		fi
	done
}

main()
{	if [ -z "$AUTHOR" ]; then 
		echo "In bash set your name: % export AUTHOR=\"Your Name\""
		exit 1
	fi
	if [ -z "$project" ]; then 
		echo "syntax: $0 PROJECT_NAME\""
		exit 1
	fi
	ReadLicenseFile
	CreateCmakeList $project
	shopt -s nullglob dotglob
#	shopt -s nullglob # Sets nullglob
# dotglob: includes filenames beginning with a ‘.’ 
# nocaseglob: matches filenames in a case-insensitive fashion
# nullglob: patterns which match no files to expand to a null string, rather than themselves
# failglob: patterns which fail to match filenames during filename expansion result in an expansion error
	AddPrograms .
	AddSubdirs .
	shopt -u nullglob # Unsets nullglob
#	cat ${cmakelist}
}

main
