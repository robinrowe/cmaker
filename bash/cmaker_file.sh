#!/bin/bash 
# cmaker_file.sh
# Created by Robin Rowe 2024/10/5
# License MIT open source

#set -x
boilerplate=$0.txt
date=$(date +%Y-%m-%d)
args=("$@")

ReadLicenseFile()
{	if [[ ! -e LICENSE ]]; then
		echo "Missing LICENSE file"
		exit 1
	fi
	read -r license < LICENSE
	echo "License: ${license}"
}

Sed()
{	local arg=$1
	local file=$2
	sed "${arg}" ${file} > ${file}.tmp
	mv -f ${file}.tmp ${file}
}

CreateFile() 
{	local src=$1
	local dst=$2
	local arg=$3
	echo Creating ${dst}...
	cp ${src} ${dst}
	Sed "s|FILE|${arg}|g" ${dst}
	Sed "s|DATE|${date}|g" ${dst}
	Sed "s|AUTHOR|${AUTHOR}|g" ${dst}
	Sed "s|LICENSE|${license}|g" ${dst}
	local prefix=${arg%.*}
	if [[ ${arg} == *".h" ]]; then
		echo "#ifndef ${prefix}_h" >> ${dst}
		echo "#define ${prefix}_h" >> ${dst}
		echo "" >> ${dst}
		echo "#endif" >> ${dst}
		return
	fi
	if [[ ${arg} == *".c" ]] || [[ ${arg} == *".cpp" ]]; then
		echo "#include \"${prefix}.h\"" >> ${dst}
		echo "" >> ${dst}
	fi
}

CreateDocs()
{	local docfile=CHANGES.md
	echo "cmaker: file ${arg}" >> ${docfile}
}

main()
{	if [ -z "$AUTHOR" ]; then 
		echo "In bash set your name: % export AUTHOR=\"Your Name\""
		exit 1
	fi
	ReadLicenseFile
	for arg in "${args[@]}"; do
		if [[ -e ${arg} ]]; then
			echo "Skipping... ${arg} already exists!"
			continue
		fi
		CreateFile ${boilerplate} "./${arg}" ${arg}
		CreateDocs ${arg}
	done
}

main




