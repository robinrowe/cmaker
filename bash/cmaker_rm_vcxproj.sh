#!/bin/bash
# cmaker_rm_vcxproj.sh
# Created by Robin.Rowe@cinepaint.org 2024/4/8
# License MIT open source

Seek() 
{	local dir=$1
    for i in "${dir}"/*;do  
		local file=$(basename $i)
		local dir=$(dirname $i)	
		if [ -f "$i" ]; then
#            echo "file: ${file}" 
#            echo "file: ${i}" 
			if [[ $file == *".vcxproj"* ]]; then
				echo "git rm $i"
				git rm $i
			fi		
			if [[ $file == *".sln" ]]; then
				echo "git rm $i"
				git rm $i
			fi				
        elif [ -d "$i" ];then 
			if [[ $i == "./build"* ]]; then
				echo "ignored: $i"
				return
			fi
			if [[ $i == "./."* ]]; then
				echo "ignored: $i"
				return
			fi
#			echo "dir: ${dir}"
			Seek "$i" 
		fi
    done 
}

echo "Git removing MSVC project files outside of ./build"
Seek .
