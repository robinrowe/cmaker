#!/bin/bash 
# cmaker_class.sh
# Created by Robin Rowe 2019/1/10
# License MIT open source

#set -x
h_file=$0.h
c_file=$0.c
test_file=$0.test.c
outstream=$0.OutStream.h
cmakelist=CMakeLists.txt
sources=sources.cmake
date=$(date +%Y-%m-%d)
basename=$1
function=$2

if [ $# -lt 3 ]; then
    >&2 echo "usage: basename \"function signature\""
    exit 1
fi

ReadLicenseFile()
{	if [[ ! -e LICENSE ]]; then
		echo "Missing LICENSE file"
		exit 1
	fi
	read -r license < LICENSE
	echo "License: ${license}"
}

Sed()
{	local arg=$1
	local file=$2
	sed "${arg}" ${file} > ${file}.tmp
	mv -f ${file}.tmp ${file}
}

CreateFile() 
{	local src=$1
	local dst=$2
	local filename=$3
	echo Creating ${dst}...
	cp ${src} ${dst}
	Sed "s|FILENAME|${basename}|g" ${dst}
	Sed "s|FUNCTION|${function}|g" ${dst}
	Sed "s|DATE|${date}|g" ${dst}
	Sed "s|AUTHOR|${AUTHOR}|g" ${dst}
	Sed "s|LICENSE|${license}|g" ${dst}
}

UpdateCmakeList()
{	local arg=$1
	echo "Updating ${cmakelist} with $arg..."
	echo "add_executable(test_${arg} test/test_${arg}.c)" >> ${cmakelist}
	echo "add_test(test_${arg} test_${arg})" >> ${cmakelist}
}

UpdateCmakeSources()
{	local arg=$1
	if [[ ! -e ${sources} ]]; then
		echo ${sources} > ${sources}
		echo "README.md" > ${sources}
		echo "CHANGES.md" > ${sources}
	fi
	echo "${arg}.h" >> ${sources}
	echo "${arg}.c" >> ${sources}
}

CreateDocs()
{	local docfile=CHANGES.md
	echo "cmaker: function ${function} in ${basname}.h" >> ${docfile}
}

main()
{	if [ -z "$AUTHOR" ]; then 
		echo "In bash set your name: % export AUTHOR=\"Your Name\""
		exit 1
	fi
	ReadLicenseFile
	if [[ ! -e test ]]; then
		mkdir test
	fi
	CreateFile ${h_file} "./${basename}.h" ${basename}
	CreateFile ${c_file} "./${basename}.c" ${basename}
	CreateFile ${test_file} "./test/test_${basename}.c" ${basename}
	UpdateCmakeList $basename
	UpdateCmakeSources $basename
	CreateDocs
}

main

