#!/bin/bash 
# cmaker_help.sh
# Created by Robin Rowe 2024/10/5
# License MIT open source

helpfile=$0.txt
date=$(date +%Y-%m-%d)
echo "${helpfile} ${date}"
cat ${helpfile}
